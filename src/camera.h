// Ján Brída xbrida01 AT stud.fit.vutbr.cz
/**
	\file camera.h
	\brief This file defines the #Camera class.

	#Camera is used as an observer of the scene. It transforms vertices to match
	their representation in Camera-space.
*/

#ifndef CAMERA_H
#define CAMERA_H

#include <vector>

#include <glm/gtc/matrix_transform.hpp>

//******************************************************************************
/**
	\class Camera
	\brief A simple camera class that can be moved using mouse/keyboard input.
	See the README file.
*/
class Camera {
public:
	/**
		\fn Camera
		\brief Constructor used especially in the #Spectator class.
		\param proj Projection matrix.
	*/
	Camera(const glm::mat4 proj) : proj{proj} { cameras.push_back(this); }
	/**
		\fn Camera
		\brief Specific constructor.
		\param proj Projection matrix.
		\param eye Position vector.
		\param forward Forward vector (which way to look at, the Z axis).
	*/
	Camera(const glm::mat4, const glm::vec3, const glm::vec3);

	/**
		\fn ~Camera
		\brief Destructor.
	*/
	virtual ~Camera(void) {}

	/**
		\fn getEye
		\brief Getter for #eye.
		\return #eye.
	*/
	const glm::vec3& getEye(void) const { return eye; }
	/**
		\fn getView
		\brief Getter for #view.
		\return #view.
	*/
	const glm::mat4& getView(void) const { return view; }
	/**
		\fn getProj
		\brief Getter for #proj. I don't think I even used it...
		\return #proj.
	*/
	const glm::mat4& getProj(void) const { return proj; }

	/**
		\fn computeMvp
		\brief Computes the model-view-projection matrix using the single
		parameter.
		\param model Model.
	*/
	glm::mat4 computeMvp(const glm::mat4&) const;
	/**
		\fn computeNormal
		\brief Computes normal matrix using the single parameter.
		
		Normal matrix is an inverse transposed model matrix in View-space. It is
		used in Phong shading model.
		\param model Model.
	*/
	glm::mat3 computeNormal(const glm::mat4&) const;

	/**
		\fn controlAll
		\brief Handles the movement of every camera in #cameras.
		\param hold Determines, if #Spectator instances should move.
	*/
	static void controlAll(const bool);

protected:
	/**
		\fn control
		\brief Virtual function implementing movement.
	*/
	virtual void control(void);
	/**
		\fn control
		\brief Used only in Spectator. Made virtual because of pointer usage in
		#controlAll.
	*/
	virtual void reset(void) {}
	/**
		\fn computeView
		\brief Computes the view matrix using member vectors.
		\return View matrix.
	*/
	glm::mat4 computeView(void) { return glm::lookAt(eye, eye + forward, up); }

	glm::vec3 eye, ///< Camera position.
	          forward, ///< Z axis.
	          right, ///< X axis.
	          up; ///< Y axis.
	glm::mat4 view; ///< Calculated view matrix.

	static const glm::vec3 globalUp; ///< This vector specifies which way the
	                                 ///< world's up vector points to.
	static const std::string keyframesDir; ///< Keyframes directory for storing
	                                       ///< keyframe data.

private:
	/**
		\enum Movement
		\brief Used with keyboard input.
	*/
	enum class Movement {FORWARD, BACK, LEFT, RIGHT};

	/**
		\fn look
		\brief Rotates the camera view.
		\param angle The rotation amount.
		\param arbitrary Sets which axis is arbitrary.
	*/
	void look(const float, const glm::vec3&);
	/**
		\fn move
		\brief Moves a camera in the given direction.
		\param movement Determines which way to move the camera.
	*/
	void move(const Movement);

	/**
		\fn currentIndex
		\brief Determines the index of a camera based on the parameter pointer in
		#cameras.
		\param camera Pointer of the queried camera.
	*/
	static unsigned currentIndex(const Camera* const);
	/**
		\fn putKeyframe
		\brief Appends another keyframe to the specified file.

		The keyframe has this format: <eye> <forward> <up> CRLF
		\param i Specifies the filename (it's the index of a camera in #cameras).
		\param eye Specifies the eye vector.
		\param forward Specifies the forward vector.
		\param up Specifies the up vector.
	*/
	static void putKeyframe(const unsigned, const glm::vec3&, const glm::vec3&,
	                        const glm::vec3&);

	const glm::mat4 proj; ///< Camera projection.

	static std::vector<Camera*> cameras; ///< Stores every constructed camera
	                                     ///< pointer.
	static const float mouseSpeed, ///< Determines mouse speed.
	                   velocity; ///< Determines movement speed.
};

//______________________________________________________________________________
inline Camera::Camera(const glm::mat4 proj, const glm::vec3 eye,
                      const glm::vec3 forward) :
                      eye{eye}, forward{glm::normalize(forward)},
                      right{glm::normalize(glm::cross(forward, globalUp))},
                      up{glm::normalize(glm::cross(right, forward))},
                      view{computeView()}, proj{proj}
{
	cameras.push_back(this);
}

//______________________________________________________________________________
inline glm::mat4 Camera::computeMvp(const glm::mat4& model) const
{
	return proj * view * model;
}

//______________________________________________________________________________
inline glm::mat3 Camera::computeNormal(const glm::mat4& model) const
{
	return glm::mat3{glm::transpose(glm::inverse(view * model))};
}

#endif