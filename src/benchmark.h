// Ján Brída xbrida01 AT stud.fit.vutbr.cz
/**
	\file benchmark.h
	\brief This file contains definitions of a singleton class named #Benchmark,
	and a nested class #Benchmark::Graph.
	
	#Benchmark class measures performance, and uses an instance of
	#Benchmark::Graph, which is derived from #Mesh, to display current
	frame rate.
*/

#ifndef BENCHMARK_H
#define BENCHMARK_H

#include <vector>
#include <list>

#include <glm/detail/type_vec2.hpp>
#include <glm/detail/type_mat4x4.hpp>

#include "mesh.h"

class Program;

//******************************************************************************
/**
	\class Benchmark
	\brief This singleton class captures the current frame rate
*/
class Benchmark {
public:
	/**
		\fn getInstance
		\brief The singleton getter.
		\return Singleton of #Benchmark.
	*/
	static Benchmark& getInstance(void);

	/**
		\fn display
		\brief This method draws the current framerate in a form of graph.
		\param p Used shader program.
	*/
	void display(Program&);
	/**
		\fn averageFramerate
		\brief This function averages the accumulated data in #frameRates.
		\return Current average frame rate.
	*/
	unsigned averageFrameRate(void);

private:
	/**
		\class Graph
		\brief This #Mesh derivative is displaying FPS graph, by feeding the graph
		with calculated data.
	*/
	class Graph : public Mesh {
	public:
		enum {SIZE = 20}; ///< Number of displayed points of a graph.

		/**
			\fn Graph
			\brief Constructor. Derives from #Mesh.
		*/
		Graph(void) : Mesh{} {}

		/**
			\fn ~Graph
			\brief Destructor.
		*/
		~Graph(void) {}

		/**
			\fn draw
			\brief Implements drawing of points.
		*/
		void draw(void);
		/**
			\fn feed
			\brief Feeds the GPU memory with data to display
			\param vertices Data to display.
		*/
		void feed(const std::vector<glm::vec2>&);

	private:
		bool firstFeed{true}; ///< Determines the first time GPU memory allocation.
	};

	/**
		\fn calc
		\brief Calculates frame rate everytime #acc hits 1.
		\return Vector of point coordinates of a graph to be displayed.
	*/
	std::vector<glm::vec2> calc(void);
	/**
		\fn decX
		\brief Decrements the single parameter's x coordinate using #stride.
		\param v 2D vector to decrement
	*/
	static void decX(glm::vec2& v) { v.x -= stride; }

	Graph fps; ///< Instance of #Benchmark::Graph used for displaying the FPS
	           ///< graph.
	float acc{0.0f}; ///< Accumulates time until it reaches 1 second.
	unsigned x{0}; ///< Current x coordinate of the new FPS data.
	std::vector<glm::vec2> data; ///< Stored FPS data.
	GLsizei count{0}; ///< Number of points to render.
	std::list<unsigned> frameRates; ///< Frame rates to be averaged.

	static const unsigned stride; ///< This determines how far from each other
	                              ///< the graph points should be.
	static const glm::mat4 proj; ///< Orthogonal projection.
};

//______________________________________________________________________________
inline Benchmark& Benchmark::getInstance(void)
{
	static Benchmark singleton;
	return singleton;
}

#endif