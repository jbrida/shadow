// Ján Brída xbrida01 AT stud.fit.vutbr.cz
/**
	\file manager.h
	\brief This file implements a handy class called #Manager to usefully create
	new objects, and deallocate them without explicitly doing so.
*/

#ifndef MANAGER_H
#define MANAGER_H

#include <map>
#include <string>

//******************************************************************************
/**
	\class Manager
	\brief This class encapsulates the management of objects of the specified
	type.
*/
template<typename T>
class Manager {
public:
	/**
		\fn Manager
		\brief Constructor.
	*/
	Manager<T>(void) {}

	/**
		\fn ~Manager
		\brief Destructor.

		Deallocates all the used up space.
	*/
	~Manager<T>(void);

	/**
		\fn Manager::operator[](const std::string name)
		\brief Returns reference to the identified item.
		\param name Identifier.
	*/
	T& operator[](const std::string name) { return *items[name]; }

	/**
		\fn isPresent
		\brief Checks if there's already a record under the specified name.
		\param name Item identifier.
	*/
	bool isPresent(const std::string) const;
	/**
		\fn add
		\brief Adds a new item into the container #items.
		\param name Item identifier.
		\param item New item.
	*/
	void add(const std::string, T* const);

private:
	std::map<std::string, T*> items; ///< Item map.
};

//______________________________________________________________________________
template<typename T>
inline Manager<T>::~Manager(void)
{
	for (typename std::map<std::string, T*>::iterator i{items.begin()}; i !=
	     items.end(); i++)
		delete i->second;
}

//______________________________________________________________________________
template<typename T>
inline bool Manager<T>::isPresent(const std::string name) const
{
	return items.find(name) != items.end();
}

//______________________________________________________________________________
template<typename T>
inline void Manager<T>::add(const std::string name, T* const item)
{
	items.insert({name, item});
}

#endif