// Ján Brída xbrida01 AT stud.fit.vutbr.cz
/**
	\file texture.h
	\brief Encapsulation of OpenGL texture object.
*/

#ifndef TEXTURE_H
#define TEXTURE_H

#include <string>
#include <stdexcept>

#include <GL/glew.h>
#include <glm/detail/type_vec2.hpp>

//******************************************************************************
/**
	\class Texture
	\brief Encapsulation of OpenGL texture object.
*/
class Texture {
public:
	/**
		\enum Format
		\brief Specifies the OpenGL texture format.
	*/
	enum Format {NONE, RGB, R32F, RG32F, RGBA16F};

	/**
		\fn Texture
		\brief Constructor.
	*/
	Texture(void);

	/**
		\fn ~Texture
		\brief Destructor.

		Calls glDeleteTextures.
	*/
	~Texture(void) { glDeleteTextures(1, &id); }

	/**
		\fn getId
		\brief Getter for #id.
		\return #id.
	*/
	GLuint getId(void) const { return id; }
	/**
		\fn getRes
		\brief Getter for #res.
		\return #res.
	*/
	const glm::ivec2& getRes(void) const { return res; }
	/**
		\fn getWidth
		\brief Returns x coordinate of #res.
		\return x coordinate of #res.
	*/
	int getWidth(void) const { return res.x; }
	/**
		\fn getHeight
		\brief Returns y coordinate of #res.
		\return y coordinate of #res.
	*/
	int getHeight(void) const { return res.y; }
	/**
		\fn getFormat
		\brief Getter for #format.
		\return #format.
	*/
	Format getFormat(void) const { return format; }

	/**
		\fn setRes
		\brief Sets #res.
		\param res New resolution.
	*/
	void setRes(const glm::ivec2& res) { this->res = res; }

	/**
		\fn bind
		\brief Binds the texture.
		\param OpenGL texture object name.
		\param Texture unit.
	*/
	static void bind(const GLuint = 0, const GLint = -1);
	/**
		\fn load
		\brief Allocates memory on GPU for a texture.
		\param format Format of the texture.
		\param filename Filename if the texture is used to cover objects.
	*/
	void load(const Format, const std::string = "");
	/**
		\fn genMip
		\brief Generates mipmaps for the currently bound texture.
	*/
	static void genMip(void) { glGenerateMipmap(GL_TEXTURE_2D); }

private:
	/**
		\struct Image
		\brief Encapsulates image data for glTexImage2D.
	*/
	struct Image {
		/**
			\fn Image
			\brief Default constructor.
		*/
		Image(void) {}
		/**
			\fn Image
			\brief Constructor.

			All parameters are concerning the OpenGL texture format.
			\param internal Internal format.
			\param format Data format.
			\param type Data type.
		*/
		Image(const GLint, const GLenum, const GLenum);

		GLint internal; ///< Internal format of texture.
		GLenum format, ///< Data format of texture.
		       type; ///< Data type of texture.
		unsigned char* data{nullptr}; ///< Data.
	};

	/**
		\fn wrap
		\brief Sets wrapping of the currently bound texture.
		\param param Parameter.
	*/
	static void wrap(const GLint);
	/**
		\fn wrap
		\brief Sets filtering of the currently bound texture.
		\param param Parameter.
	*/
	static void filter(const GLint);
	/**
		\fn anisotropy
		\brief Sets the highest anisotropy of the currently bound texture.
	*/
	static void anisotropy(void);

	GLuint id; ///< OpenGL texture object name.
	glm::ivec2 res; ///< Texture resolution.
	Format format{Format::NONE}; ///< Texture format.

	static const std::string imageDir; ///< Image directory.
};

//______________________________________________________________________________
inline Texture::Texture(void)
{
	glGenTextures(1, &id);
	if (!id)
		throw std::runtime_error{"glGenTexture: unable to create object"};
}

//______________________________________________________________________________
inline void Texture::bind(const GLuint id, const GLint i)
{
	static GLuint bound{0};
	static GLint activated{0};
	bool switched{false};
	if (i >= 0 && i != activated) {
		glActiveTexture(GL_TEXTURE0 + i);
		activated = i;
		switched = true;
	}
	if (id != bound || switched) {
		glBindTexture(GL_TEXTURE_2D, id);
		bound = id;
	}
}

//______________________________________________________________________________
inline Texture::Image::Image(const GLint internal, const GLenum format,
                             const GLenum type) :
                             internal{internal}, format{format}, type{type}
{
}

//______________________________________________________________________________
inline void Texture::wrap(const GLint param)
{
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, param);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, param);
}

//______________________________________________________________________________
inline void Texture::filter(const GLint param)
{
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, param);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, param);
}

//______________________________________________________________________________
inline void Texture::anisotropy(void) {
	static GLfloat a{0.0f};
	if (!a)
		glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &a);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, a);
}

#endif