// Ján Brída xbrida01 AT stud.fit.vutbr.cz

#include <fstream>
#include <stdexcept>

#include <glm/gtx/rotate_vector.hpp>

#include "camera.h"
#include "input.h"
#include "time.h"

//______________________________________________________________________________
void Camera::controlAll(const bool hold)
{
	static Camera* active{nullptr};

	for (unsigned i{0}; i < Input::SIZE - Input::F1; i++)
		if (Input::getInstance().isKeyDown(static_cast<Input::Key>
		                                   (Input::F1 + i))) {
		active = cameras.at(i);
		break;
	}

	if (Input::getInstance().isKeyDown(Input::R)) {
		active = nullptr;
		for (Camera* c : cameras)
			c->reset();
	}

	for (Camera* c : cameras)
		if (c == active)
			c->Camera::control();
		else if (!hold)
			c->control();

	if (Input::getInstance().isKeyDown(Input::K))
		putKeyframe(currentIndex(active), active->eye, active->forward, active->up);
}

//______________________________________________________________________________
void Camera::control(void)
{
	const glm::vec2 cursorDelta{Input::getInstance().getCursorDelta()};
	if (cursorDelta.x != 0.0f)
		look(cursorDelta.x, globalUp);
	if (cursorDelta.y != 0.0f)
		look(cursorDelta.y, right);

	if (Input::getInstance().isKeyDown(Input::W))
		move(Movement::FORWARD);
	if (Input::getInstance().isKeyDown(Input::S))
		move(Movement::BACK);
	if (Input::getInstance().isKeyDown(Input::A))
		move(Movement::LEFT);
	if (Input::getInstance().isKeyDown(Input::D))
		move(Movement::RIGHT);
}

//______________________________________________________________________________
void Camera::look(const float angle, const glm::vec3& arbitrary)
{
	forward = glm::normalize(glm::rotate(forward, angle * mouseSpeed *
	                                     Time::getInstance().getDelta(),
	                                     arbitrary));
	right = glm::normalize(glm::cross(forward, globalUp));
	up = glm::normalize(glm::cross(right, forward));

	view = computeView();
}

//______________________________________________________________________________
void Camera::move(const Movement movement)
{
	switch (movement) {
		case Movement::FORWARD:
			eye += forward * velocity * Time::getInstance().getDelta();
			break;
		case Movement::BACK:
			eye -= forward * velocity * Time::getInstance().getDelta();
			break;
		case Movement::LEFT:
			eye -= right * velocity * Time::getInstance().getDelta();
			break;
		case Movement::RIGHT:
			eye += right * velocity * Time::getInstance().getDelta();
		default:
			break;
	}

	view = computeView();
}

//______________________________________________________________________________
unsigned Camera::currentIndex(const Camera* const camera)
{
	for (unsigned i{0}; i < cameras.size(); i++)
		if (cameras[i] == camera)
			return i + 1;
	return 1;
}

//______________________________________________________________________________
void Camera::putKeyframe(const unsigned i, const glm::vec3& eye,
                         const glm::vec3& forward, const glm::vec3& up)
{
	static glm::vec3 last;
	if (last == eye)
		return;
	last = eye;

	std::ofstream output{keyframesDir + std::to_string(i), std::ios::app};
	if (!output.is_open())
		throw std::runtime_error{"std::ofstream::is_open: output error"};

	output << eye.x << ' ' << eye.y << ' ' << eye.z << ' ' << forward.x << ' ' <<
	forward.y << ' ' << forward.z << ' ' << up.x << ' ' << up.y << ' ' << up.z <<
	std::endl;
	output.close();
}

//------------------------------------------------------------------------------
const std::string Camera::keyframesDir{"keyframes/"};
const glm::vec3 Camera::globalUp{0.0f, 1.0f, 0.0f};
std::vector<Camera*> Camera::cameras;
const float Camera::mouseSpeed{3.0f}, Camera::velocity{10.0f};