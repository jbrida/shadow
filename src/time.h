// Ján Brída xbrida01 AT stud.fit.vutbr.cz
/**
	\file time.h
	\brief This file contains definitions of a singleton class named #Time, and a
	nested class #Time::Timer.
	
	They are used for calculating the time needed to render a displayed frame, and
	to count down to an event respectively.
*/

#ifndef TIME_H
#define TIME_H

#include <GLFW/glfw3.h>

//******************************************************************************
/**
	\class Time
	\brief A singleton class that keeps track of the time needed for a single
	frame rendering.

	Call its member method #update at the start of each cycle of the main render
	loop for desired effect. You can query the time needed for a single frame
	rendering by invoking the getter #getDelta.
*/
class Time {
public:
	/**
		\class Timer
		\brief Used for counting down to a custom event.
	*/
	class Timer {
	public:
		/**
			\fn Timer
			\brief Constructor.
			\param limit Specifies how long to countdown.
		*/
		Timer(const float limit) : limit{limit} {}

		/**
			\fn ~Timer
			\brief Destructor.
		*/
		~Timer(void) {}

		/**
			\fn isReady
			\brief Check if #ready is set to true.
		*/
		bool isReady(void) { return ready; }
		/**
			\fn Timer
			\brief Starts the timer.
			\return State of #ready.
		*/
		void start(void) { ready = false; }
		/**
			\fn update
			\brief Updates the timer.
		*/
		void update(void);

	private:
		bool ready{true}; ///< States if an instance is allowed to count down.
		float acc{0.0f}; ///< Accumulates time passed since the start of the
		                 ///< countdown.
		const float limit; ///< Specifies how long to count down.
	};

	/**
		\fn getInstance
		\brief The singleton getter.
		\return Singleton of #Time.
	*/
	static Time& getInstance(void);

	/**
		\fn update
		\brief Calculates time passed since the last invocation.
	*/
	void update(void);
	/**
		\fn getDelta
		\brief Getter function for the variable #delta.
		\return Amount of time passed since the last invocation of #update.
	*/
	float getDelta(void) const { return delta; }

private:
	/**
		\fn Time
		\brief Constructor.
		\param before The first recorded time value (use the glfwGetTime function).
	*/
	Time(const float before) : before{before} {}

	float before, ///< Time to substract from the current time to get time delta.
	       delta; ///< Time needed for a single frame rendering.
};

//______________________________________________________________________________
inline void Time::Timer::update(void)
{
	if (!ready) {
		acc += Time::getInstance().getDelta();
		if (acc >= limit) {
			acc = 0.0f;
			ready = true;
		}
	}
}

//______________________________________________________________________________
inline Time& Time::getInstance(void)
{
	static Time singleton(glfwGetTime());
	return singleton;
}

//______________________________________________________________________________
inline void Time::update(void)
{
	const float now(glfwGetTime());
	delta = now - before;
	before = now;
}

#endif