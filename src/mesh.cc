// Ján Brída xbrida01 AT stud.fit.vutbr.cz

#include <vector>
#include <stdexcept>

#include <glm/detail/type_vec3.hpp>
#include <glm/detail/type_vec2.hpp>
#include <GL/glew.h>

#include "mesh.h"

//______________________________________________________________________________
Mesh::Mesh(void)
{
	glGenVertexArrays(1, &objects[VERTEX_ARRAY]);
	if (!objects[VERTEX_ARRAY])
		throw std::runtime_error{"glGenVertexArrays: unable to create object"};
	glGenBuffers(2, &objects[VERTEX_BUFFER]);
	if (!objects[VERTEX_BUFFER] || !objects[ELEMENT_BUFFER])
		throw std::runtime_error{"glGenBuffers: unable to create object"};
}

//______________________________________________________________________________
Mesh::Mesh(const std::vector<Vertex>& vertices,
           const std::vector<glm::uvec3>& indices) :
           count(indices.size())
{
	glGenVertexArrays(1, &objects[VERTEX_ARRAY]);
	if (!objects[VERTEX_ARRAY])
		throw std::runtime_error{"glGenVertexArrays: unable to create object"};
	glGenBuffers(2, &objects[VERTEX_BUFFER]);
	if (!objects[VERTEX_BUFFER] || !objects[ELEMENT_BUFFER])
		throw std::runtime_error{"glGenBuffers: unable to create object"};

	glBindVertexArray(objects[VERTEX_ARRAY]);

	glBindBuffer(GL_ARRAY_BUFFER, objects[VERTEX_BUFFER]);
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vertex), &vertices[0],
	             GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, objects[ELEMENT_BUFFER]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, count * sizeof(glm::uvec3), &indices[0],
	             GL_STATIC_DRAW);

	glEnableVertexAttribArray(Vertex::POSITION);
	glVertexAttribPointer(Vertex::POSITION,
	                      sizeof(glm::vec3) / sizeof(GLfloat),
	                      GL_FLOAT,
	                      GL_FALSE,
	                      sizeof(Vertex),
	                      reinterpret_cast<GLvoid*>(offsetof(Vertex, position)));

	glEnableVertexAttribArray(Vertex::NORMAL);
	glVertexAttribPointer(Vertex::NORMAL,
	                      sizeof(glm::vec3) / sizeof(GLfloat),
	                      GL_FLOAT,
	                      GL_FALSE,
	                      sizeof(Vertex),
	                      reinterpret_cast<GLvoid*>(offsetof(Vertex, normal)));

	glEnableVertexAttribArray(Vertex::UV);
	glVertexAttribPointer(Vertex::UV,
	                      sizeof(glm::vec2) / sizeof(GLfloat),
	                      GL_FLOAT,
	                      GL_FALSE,
	                      sizeof(Vertex),
	                      reinterpret_cast<GLvoid*>(offsetof(Vertex, uv)));

	glBindVertexArray(0);
}

//______________________________________________________________________________
void Mesh::draw(void)
{
	glBindVertexArray(objects[VERTEX_ARRAY]);
	glDrawElements(GL_TRIANGLES, count * 3, GL_UNSIGNED_INT, nullptr);
	glBindVertexArray(0);
}