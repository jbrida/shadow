// Ján Brída xbrida01 AT stud.fit.vutbr.cz
/**
	\file filter.h
	\brief The #Filter class is defined here.
*/

#ifndef FILTER_H
#define FILTER_H

#include "technique.h"
#include "offscreen.h"
#include "program.h"

//******************************************************************************
/**
	\class Filter
	\brief This class implements a blur method to reduce aliasing artifacts in
	shadow map.
*/
class Filter : public Technique, public Offscreen {
public:
	/**
		\enum Stage
		\brief This type specifies different stages of convolving the shadow map.
	*/
	enum class Stage {ON_X, ON_Y, SHARE /**< Used to bind #input only.*/};

	/**
		\fn Filter
		\brief Constructor.
		\param input Source texture.
	*/
	Filter(Texture&);

	/**
		\fn ~Filter
		\brief Destructor.
	*/
	~Filter(void) {}

	/**
		\fn prologue
		\brief Implements texture binding based on the current stage #curr.
	*/
	void prologue(void);
	/**
		\fn apply
		\brief Does nothing.
		\param model Model.
	*/
	void apply(__attribute__((__unused__)) const glm::mat4&) {}

	/**
		\fn setCurr
		\brief Sets #curr.
		\param curr New #curr.
	*/
	void setCurr(const Stage curr) { this->curr = curr; }

private:
	Texture& input; ///< Source texture to blur.
	Stage curr{Stage::ON_X}; ///< Determines the state of the filtering procedure.
};

//______________________________________________________________________________
Filter::Filter(Texture& input) :
               Technique{}, Offscreen{new Framebuffer{input.getRes()}},
               input{input}
{
	gbuffer->attach(input.getFormat());
	texUnit = 0;
}

//______________________________________________________________________________
void Filter::prologue(void)
{
	switch (curr) {
		case Stage::ON_X:
			Program::getUsed().setUniform("BLUR", glm::vec2{1.0f / input.getRes().x,
			                                                0.0f});
			Program::getUsed().setUniform("INPUT", texUnit);
			Texture::bind(input.getId(), texUnit);
			curr = Stage::ON_Y;
			break;
		case Stage::ON_Y:
			curr = Stage::ON_X;
			Program::getUsed().setUniform("BLUR", glm::vec2{0.0f, 1.0f /
			                                                input.getRes().y});
			Texture::bind(gbuffer->getTexture().getId(), texUnit);
			break;
		default:
			Program::getUsed().setUniform("INPUT", texUnit);
			Texture::bind(input.getId(), texUnit);
			curr = Stage::ON_X;
			break;
	}
}

#endif