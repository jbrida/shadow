// Ján Brída xbrida01 AT stud.fit.vutbr.cz
/**
	\file spectator.h
	\brief This file defines the #Spectator class, a derivative of #Camera.

	#Spectator is used for moving camera view using keyframe data.
*/

#ifndef SPECTATOR_H
#define SPECTATOR_H

#include <string>
#include <vector>

#include "camera.h"

//******************************************************************************
/**
	\class Spectator
	\brief This class implements camera movement without user input.

	It basicaly computes all required vectors needed to calculate the view matrix
	from data, that is stored in keyframe files in the keyframes/ directory.
*/
class Spectator : public Camera {
public:
	/**
		\fn Spectator
		\brief Constructor.
		\param proj Projection matrix.
		\param filename %Keyframe file.
	*/
	Spectator(const glm::mat4, const std::string);

	/**
		\fn ~Spectator
		\brief Destructor.
	*/
	~Spectator(void) {}

private:
	/**
		\struct Keyframe
		\brief Simple struct that encapsulates each line in a keyframe file.
	*/
	struct Keyframe {
		glm::vec3 v[3]; ///< %Keyframe data: eye, forward, up.
	};

	/**
		\fn control
		\brief Implementation of movement based on keyframe data.
	*/
	void control(void);
	/**
		\fn reset
		\brief Resets the route.
	*/
	void reset(void) { index = 0; alpha = 0.0f; }

	/**
		\fn loadKeyframes
		\brief This function loads keyframe data from a file.
		\param filename %Keyframe file.
		\return All keyframe data that was present in the file.
	*/
	static std::vector<Keyframe> loadKeyframes(const std::string&);

	std::vector<Keyframe> keyframes; ///< Processed keyframe data.
	unsigned index{0}; ///< Specifies the current keyframe in #keyframes.
	float alpha{0.0f}; ///< Used for interpolation.

	static const float speedReduction; ///< Simply reduces the movement speed.
};

//______________________________________________________________________________
inline Spectator::Spectator(const glm::mat4 proj, const std::string filename) :
                            Camera{proj}, keyframes{loadKeyframes(filename)}
{
	eye = keyframes[index].v[0];
	forward = keyframes[index].v[1];
	up = keyframes[index].v[2];

	view = computeView();
}

#endif