// Ján Brída xbrida01 AT stud.fit.vutbr.cz

#include <stdexcept>
#include <iostream>
#include <iomanip>

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/detail/type_mat4x4.hpp>

#include "renderer.h"
#include "input.h"
#include "program.h"
#include "camera.h"
#include "spectator.h"
#include "phong.h"
#include "depth_map.h"
#include "filter.h"
#include "scene.h"
#include "time.h"
#include "benchmark.h"

//______________________________________________________________________________
Renderer::Renderer(void)
{
	// GLFW initialization.
	if (!glfwInit())
		throw std::runtime_error{"glfwInit: failed to initialize"};
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
	clean = Clean::GLFW;

	// Create window, and apply callbacks.
	window = glfwCreateWindow(res.x, res.y, "Shadow", nullptr, nullptr);
	if (!window)
		throw std::runtime_error{"glfwCreateWindow: nullptr returned"};
	glfwMakeContextCurrent(window);
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
	glfwSetCursorPosCallback(window, Input::cursorPosCallback);
	glfwSetKeyCallback(window, Input::keyCallback);
	clean = Clean::WINDOW;

	// Set up GLEW.
	glewExperimental = GL_TRUE;
	if (glewInit() != GLEW_OK)
		throw std::runtime_error{"glewInit: failed to initialize"};
	if (!GLEW_VERSION_4_2)
		throw std::runtime_error{"GLEW_VERSION_4_2: not defined"};

	// OpenGL settings.
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
}

//______________________________________________________________________________
Renderer::~Renderer(void)
{
	switch (clean) {
		case Clean::WINDOW:
			glfwDestroyWindow(window);
		case Clean::GLFW:
			glfwTerminate();
		default:
			break;
	}
}

//______________________________________________________________________________
void Renderer::initPrograms()
{
	std::cerr << "COMPILING \"shadow\":" << std::endl;
	programs.add("shadow", new Program{"shadow.vert", "shadow.frag"});
	std::cerr << "COMPILING \"shadow-vsm\":" << std::endl;
	programs.add("shadow-vsm", new Program{"shadow.vert", "shadow-vsm.frag"});
	std::cerr << "COMPILING \"shadow-esm\":" << std::endl;
	programs.add("shadow-esm", new Program{"shadow.vert", "shadow-esm.frag"});
	std::cerr << "COMPILING \"gauss\":" << std::endl;
	programs.add("gauss", new Program{"gauss.vert", "gauss.frag"});
	std::cerr << "COMPILING \"basis\":" << std::endl;
	programs.add("basis", new Program{"basis.vert", "basis.frag"});
	std::cerr << "COMPILING \"object-pcf\":" << std::endl;
	programs.add("object-pcf", new Program{"object.vert", "object-pcf.frag"});
	std::cerr << "COMPILING \"object-vsm\":" << std::endl;
	programs.add("object-vsm", new Program{"object.vert", "object-vsm.frag"});
	std::cerr << "COMPILING \"object-csm\":" << std::endl;
	programs.add("object-csm", new Program{"object.vert", "object-csm.frag"});
	std::cerr << "COMPILING \"object-xcsm\":" << std::endl;
	programs.add("object-xcsm", new Program{"object.vert", "object-xcsm.frag"});
	std::cerr << "COMPILING \"object-esm\":" << std::endl;
	programs.add("object-esm", new Program{"object.vert", "object-esm.frag"});
	std::cerr << "COMPILING \"plot\":" << std::endl;
	programs.add("plot", new Program{"plot.vert", "plot.frag"});
}

//______________________________________________________________________________
void Renderer::initCameras()
{
	cameras.add("observer", new Spectator{glm::perspective(45.0f,
	                                      static_cast<float>(res.x / res.y),
	                                      0.1f, 100.0f), "1"});
	cameras.add("caster", new Spectator{glm::perspective(45.0f,
	                                    static_cast<float>(mapRes.x / mapRes.y),
	                                    1.0f, 50.0f), "2"});
}

//______________________________________________________________________________
void Renderer::initTechniques()
{
	techniques.add("phong", new Phong{cameras["caster"], cameras["observer"]});
	techniques.add("depth", new DepthMap{new Framebuffer{mapRes}, Texture::R32F,
	                                     cameras["caster"]});
	techniques.add("moments", new DepthMap{new Framebuffer{mapRes},
	                                       Texture::RG32F, cameras["caster"]});
	techniques.add("terms", new DepthMap{new Framebuffer{mapRes, 8},
	                                     Texture::RGBA16F, cameras["caster"]});
	techniques.add("blur-depth", new Filter{dynamic_cast<Offscreen*>
	                                        (&techniques["depth"])->
	                                        getGbuffer().getTexture()});
	techniques.add("blur-moments", new Filter{dynamic_cast<Offscreen*>
	                                          (&techniques["moments"])->
	                                          getGbuffer().getTexture()});
}

//______________________________________________________________________________
void Renderer::loop(void)
{
	std::cout << "METHOD: FPS | FRAME TIME" << std::endl;

	Scene test{"sibenik/sibenik.obj"};
	while (!glfwWindowShouldClose(window)) {
		Time::getInstance().update();
		options();
		Camera::controlAll(hold);

		switch (select()) {
			case State::PCF:
				shadowPass(test, programs["shadow"], &techniques["depth"]);
				objectPass(test, programs["object-pcf"], &techniques["depth"]);
				break;
			case State::VSM:
				shadowPass(test, programs["shadow-vsm"], &techniques["moments"]);
				if (conv)
					gaussPass(test, &techniques["blur-moments"], &techniques["moments"]);
				objectPass(test, programs["object-vsm"], &techniques["moments"]);
				break;
			case State::CSM:
				shadowPass(test, programs["shadow"], &techniques["depth"]);
				if (conv)
					gaussPass(test, &techniques["blur-depth"], &techniques["depth"]);
				basisPass(test);
				objectPass(test, programs["object-csm"], &techniques["terms"]);
				break;
			case State::XCSM:
				shadowPass(test, programs["shadow"], &techniques["depth"]);
				if (conv)
					gaussPass(test, &techniques["blur-depth"], &techniques["depth"]);
				objectPass(test, programs["object-xcsm"], &techniques["depth"]);
				break;
			case State::ESM:
				shadowPass(test, programs["shadow-esm"], &techniques["depth"]);
				if (conv)
					gaussPass(test, &techniques["blur-depth"], &techniques["depth"]);
				objectPass(test, programs["object-esm"], &techniques["depth"]);
				break;
			default:
				glfwSetWindowShouldClose(window, GL_TRUE);
				break;
		}

		casterView(test);
		plotPass();

		glfwSwapBuffers(window);
		glfwPollEvents();
	}
}

//______________________________________________________________________________
void Renderer::options(void)
{
	static Time::Timer delays[2]{0.5f, 0.5f};

	if (delays[0].isReady() && Input::getInstance().isKeyDown(Input::H)) {
		hold = !hold;
		delays[0].start();
	}
	if (delays[1].isReady() && Input::getInstance().isKeyDown(Input::G)) {
		conv = !conv;
		delays[1].start();
	}

	delays[0].update();
	delays[1].update();
}

//______________________________________________________________________________
Renderer::State Renderer::select(void)
{
	static State selected{State::PCF};

	const State prev{selected};
	if (Input::getInstance().isKeyDown(Input::ESCAPE))
		selected = State::EXIT;
	else if (Input::getInstance().isKeyDown(Input::P))
		selected = State::PCF;
	else if (Input::getInstance().isKeyDown(Input::V))
		selected = State::VSM;
	else if (Input::getInstance().isKeyDown(Input::C))
		selected = State::CSM;
	else if (Input::getInstance().isKeyDown(Input::X))
		selected = State::XCSM;
	else if (Input::getInstance().isKeyDown(Input::E))
		selected = State::ESM;

	if (selected != prev)
		showStats(prev);

	return selected;
}

//______________________________________________________________________________
void Renderer::showStats(const State prev)
{
	switch (prev) {
		case State::PCF:
			std::cout << "   PCF: ";
			break;
		case State::VSM:
			std::cout << "   VSM: ";
			break;
		case State::CSM:
			std::cout << "   CSM: ";
			break;
		case State::XCSM:
			std::cout << "  XCSM: ";
			break;
		case State::ESM:
			std::cout << "   ESM: ";
		default:
			break;
	}

	const unsigned avg{Benchmark::getInstance().averageFrameRate()};
	if (avg > 0) {
		if (avg < 100)
			std::cout << " ";
		if (avg < 10)
			std::cout << " ";
		std::cout << avg << " | " << std::fixed << std::setprecision(8) <<
		1.0f / static_cast<float>(avg);
	}
	else
		std::cout << "not enough data!";

	std::cout << std::endl;
}

//______________________________________________________________________________
void Renderer::shadowPass(Scene& s, Program& p, Technique* const t)
{
	viewport(dynamic_cast<Offscreen*>(t)->getGbuffer().getTexture().getRes());
	Framebuffer::bind(dynamic_cast<Offscreen*>(t)->getGbuffer().getId());
	clear();
	s.renderAll(p, cameras["caster"]);
}

//______________________________________________________________________________
void Renderer::gaussPass(Scene& s, Technique* const f, Technique* const a)
{
	Framebuffer::bind(dynamic_cast<Offscreen*>(f)->getGbuffer().getId());
	clear();
	s.renderAll(programs["gauss"], cameras["caster"], {f});

	Framebuffer::bind(dynamic_cast<Offscreen*>(a)->getGbuffer().getId());
	clear();
	s.renderAll(programs["gauss"], cameras["caster"], {f});
}

//______________________________________________________________________________
void Renderer::basisPass(Scene& s)
{
	Framebuffer::bind(dynamic_cast<Offscreen*>(&techniques["terms"])->
	                                           getGbuffer().getId());
	drawBuffers(dynamic_cast<Offscreen*>(&techniques["terms"])->
	                                     getGbuffer().getSize() - 1);
	clear();
	dynamic_cast<Filter*>(&techniques["blur-depth"])->
	                      setCurr(Filter::Stage::SHARE);
	s.renderAll(programs["basis"], cameras["caster"],
	            {&techniques["blur-depth"]});
}

//______________________________________________________________________________
void Renderer::objectPass(Scene& s, Program& p, Technique* const t)
{
	viewport(res);
	Framebuffer::bind();
	drawBuffers();
	clear();
	s.renderAll(p, cameras["observer"], {&techniques["phong"], t}, true);
}

//______________________________________________________________________________
void Renderer::casterView(Scene& s)
{
	scissorDepth(res / 4);
	clear(false);
	viewport(res / 4);
	s.renderAll(programs["shadow"], cameras["caster"]);
}

//______________________________________________________________________________
void Renderer::plotPass(void)
{
	scissorDepth(res / 4, res - glm::ivec2{res.x / 4, res.y / 4});
	clear(false);
	viewport(res / 4, res - glm::ivec2{res.x / 4, res.y / 4});
	Benchmark::getInstance().display(programs["plot"]);
}

//______________________________________________________________________________
void Renderer::viewport(const glm::ivec2 res, const glm::ivec2 origin)
{
	glViewport(origin.x, origin.y, res.x, res.y);
}

//______________________________________________________________________________
void Renderer::clear(const bool both)
{
	GLbitfield mask{GL_DEPTH_BUFFER_BIT};
	if (both)
		mask |= GL_COLOR_BUFFER_BIT;
	glClear(mask);
}

//______________________________________________________________________________
void Renderer::drawBuffers(const unsigned n)
{
	std::vector<GLenum> bufs;
	for (unsigned i{0}; i <= n; i++)
		bufs.push_back(GL_COLOR_ATTACHMENT0 + i);
	glDrawBuffers(bufs.size(), &bufs[0]);
}

//______________________________________________________________________________
void Renderer::scissorDepth(const glm::ivec2 res, const glm::ivec2 origin)
{
	glEnable(GL_SCISSOR_TEST);
	glScissor(origin.x, origin.y, res.x / 4, res.y / 4);
	clear(false);
	glDisable(GL_SCISSOR_TEST);
}