// Ján Brída xbrida01 AT stud.fit.vutbr.cz

#include <stdexcept>
#include <iostream>
#include <fstream>
#include <streambuf>
#include <GL/glew.h>
#include "program.h"

//______________________________________________________________________________
Program::Program(const std::string vert, const std::string frag)
{
	if (!id)
		throw std::runtime_error{"glCreateProgram: unable to create object"};

	link({{GL_VERTEX_SHADER, *this, loadSource(vert)},
	      {GL_FRAGMENT_SHADER, *this, loadSource(frag)}});
}

//______________________________________________________________________________
Program::Shader::Shader(const GLenum type, const Program& program,
                        const GLchar* const source) :
                        id{glCreateShader(type)}, program{program}
{
	if (!id)
		throw std::runtime_error{"glCreateShader: unable to create object"};

	glShaderSource(id, 1, &source, nullptr);
	glCompileShader(id);
	errorCheck();
}

//______________________________________________________________________________
void Program::Shader::errorCheck(void) const
{
	GLchar log[logger::SIZE];
	glGetShaderInfoLog(id, sizeof(log), nullptr, log);
	std::cerr << log;

	GLint status;
	glGetShaderiv(id, GL_COMPILE_STATUS, &status);
	if (!status)
		throw std::runtime_error{"glGetShaderiv: error on compile"};
}

//______________________________________________________________________________
void Program::link(std::list<Shader> shaders)
{
	for (Shader& s : shaders)
		s.attach();
	glLinkProgram(id);
	errorCheck();
}

//______________________________________________________________________________
void Program::errorCheck(void) const
{
	GLchar log[logger::SIZE];
	glGetProgramInfoLog(id, sizeof(log), nullptr, log);
	std::cerr << log;

	GLint status;
	glGetProgramiv(id, GL_LINK_STATUS, &status);
	if (!status)
		throw std::runtime_error{"glGetProgramiv: cannot link shaders"};

	glValidateProgram(id);
	glGetProgramInfoLog(id, sizeof(log), nullptr, log);
	std::cerr << log << std::endl;

	glGetProgramiv(id, GL_VALIDATE_STATUS, &status);
	if (!status)
		throw std::runtime_error{"glGetProgramiv: invalid shader program"};
}

//______________________________________________________________________________
const GLchar* Program::loadSource(const std::string& filename)
{
	std::ifstream input{shaderDir + filename};
	if (!input.is_open())
		throw std::runtime_error{"std::ifstream::is_open: input error on \"" +
		                         filename + "\""};
	const std::string content{{std::istreambuf_iterator<char>(input)},
	                          std::istreambuf_iterator<char>()};
	input.close();
	return content.c_str();
}

//------------------------------------------------------------------------------
Program* Program::used{nullptr};
const std::string Program::shaderDir{"shaders/"};