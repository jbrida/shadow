// Ján Brída xbrida01 AT stud.fit.vutbr.cz

#include "framebuffer.h"

//______________________________________________________________________________
Framebuffer::Framebuffer(const glm::ivec2& res, const unsigned n) : textures{n}
{
	glGenFramebuffers(1, &id);
	if (!id)
		throw std::runtime_error{"glGenFramebuffers: unable to create object"};

	for (Texture& t : textures)
		t.setRes(res);
}

//______________________________________________________________________________
void Framebuffer::read(unsigned i)
{
	for (Texture& t : textures) {
		Texture::bind(t.getId(), i++);
		Texture::genMip();
	}
}

//______________________________________________________________________________
void Framebuffer::attach(const Texture::Format format)
{
	bind(id);

	for (unsigned i{0}; i < textures.size(); i++) {
		textures[i].load(format);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + i,
		                       GL_TEXTURE_2D, textures[i].getId(), 0);
	}

	renderbuffer.storage(textures[0].getWidth(), textures[0].getHeight());
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,
		                        GL_RENDERBUFFER, renderbuffer.getId());

	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		throw std::runtime_error{"glCheckFramebufferStatus: incomplete FBO"};
	bind();
}