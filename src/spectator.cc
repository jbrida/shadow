#include <fstream>
#include <stdexcept>
#include <sstream>

#include "spectator.h"
#include "time.h"

//______________________________________________________________________________
void Spectator::control(void)
{
	if (index + 1 == keyframes.size()) {
		index = 0;
		eye = keyframes[index].v[0];
		forward = keyframes[index].v[1];
		up = keyframes[index].v[2];
	}

	alpha += speedReduction * Time::getInstance().getDelta();
	if (alpha > 1.0f)
		alpha = 1.0f;

	eye = glm::mix(keyframes[index].v[0], keyframes[index + 1].v[0], alpha);
	forward = glm::mix(keyframes[index].v[1], keyframes[index + 1].v[1], alpha);
	up = glm::mix(keyframes[index].v[2], keyframes[index + 1].v[2], alpha);

	if (alpha == 1.0f) {
		alpha = 0.0f;
		index++;
	}

	view = computeView();
}

//______________________________________________________________________________
std::vector<Spectator::Keyframe> Spectator::loadKeyframes(const std::string&
                                                          filename)
{
	std::ifstream input{keyframesDir + filename};
	if (!input.is_open())
		throw std::runtime_error{"std::ifstream::is_open: input error on \"" +
		                         filename + "\""};

	std::vector<Keyframe> keyframes;
	for (std::string line; std::getline(input, line);) {
		std::istringstream data(line);
		Keyframe keyframe;
		for (unsigned i{0}; i < 3; i++)
			data >> keyframe.v[i].x >> keyframe.v[i].y >> keyframe.v[i].z;
		keyframes.push_back(keyframe);
	}
	input.close();

	return keyframes;
}

//______________________________________________________________________________
const float Spectator::speedReduction{0.5f};