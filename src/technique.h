// Ján Brída xbrida01 AT stud.fit.vutbr.cz
/**
	\file technique.h
	\brief Abstract class #Technique.
*/

#ifndef TECHNIQUE_H
#define TECHNIQUE_H

#include <glm/detail/type_mat4x4.hpp>

//******************************************************************************
/**
	\class Technique
	\brief This abstract class is used to provide an interface for OpenGL uniform
	handling based on the #prologue, and #apply implementations.
*/
class Technique {
public:
	/**
		\fn Technique
		\brief Constructor.
	*/
	Technique(void) {}

	/**
		\fn ~Technique
		\brief Destructor.
	*/
	virtual ~Technique(void) {}

	/**
		\fn prologue
		\brief Called only once during a mesh render loop.
	*/
	virtual void prologue(void) = 0;
	/**
		\fn apply
		\brief Applies calculations to the supplied model,
		typically to create transformations.
		\param model Model.
	*/
	virtual void apply(const glm::mat4&) = 0;
};

#endif