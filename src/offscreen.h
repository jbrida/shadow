// Ján Brída xbrida01 AT stud.fit.vutbr.cz
/**
	\file offscreen.h
	\brief Implementation of #Offscreen.
*/

#ifndef OFFSCREEN_H
#define OFFSCREEN_H

#include <vector>
#include <memory>

#include "framebuffer.h"

//******************************************************************************
/**
	\class Offscreen
	\brief This class encapsulates handling FBO during shadow mapping.
*/
class Offscreen {
public:
	/**
		\fn Offscreen
		\brief Constructor.
		\param gbuffer New framebuffer.
	*/
	Offscreen(Framebuffer* const gbuffer) : gbuffer{gbuffer} {}

	/**
		\fn ~Offscreen
		\brief Destructor.
	*/
	virtual ~Offscreen(void) {}

	/**
		\fn getGbuffer
		\brief Getter for #gbuffer.
		\return #gbuffer.
	*/
	Framebuffer& getGbuffer(void) { return *gbuffer; }

	/**
		\fn setTexUnit
		\brief Setter for #texUnit.
		\param texUnit New #texUnit.
	*/
	void setTexUnit(const GLint texUnit) { this->texUnit = texUnit; }

protected:
	/**
		\fn texUnits
		\brief Calculates texture units based on #texUnit (by incrementing
		based on the FBO size).
		\return Vector of calculated texture units.
	*/
	std::vector<GLint> texUnits(void);

	std::unique_ptr<Framebuffer> gbuffer; ///< FBO.
	GLint texUnit; ///< Starting texture unit.
};

//______________________________________________________________________________
std::vector<GLint> Offscreen::texUnits(void)
{
	std::vector<GLint> texUnits;
	for (unsigned i{0}; i < gbuffer->getSize(); i++)
		texUnits.push_back(texUnit + i);
	return texUnits;
}

#endif