// Ján Brída xbrida01 AT stud.fit.vutbr.cz
/**
	\file mesh.h
	\brief Mesh class implementation.
*/

#ifndef MESH_H
#define MESH_H

#include <vector>

#include <GL/glew.h>
#include <glm/detail/type_vec3.hpp>
#include <glm/detail/type_vec2.hpp>

//******************************************************************************
/**
	\class Mesh
	\brief This class represents vertex array/buffer/element objects. It is used
	to draw simple objects.
*/
class Mesh {
public:
	/**
		\struct Vertex
		\brief Simple class to encapsulate vertex.
	*/
	struct Vertex {
		/**
			Represents input layout in shader programs.
		*/
		enum {POSITION, NORMAL, UV};

		/**
			\fn Vertex
			\brief Constructor.
			\param position Vertex position.
			\param normal Vertex normal.
			\param uv Vertex UV coordinates.
		*/
		Vertex(const glm::vec3, const glm::vec3, const glm::vec2);

		glm::vec3 position, ///< Vertex position.
		          normal; ///< Vertex normal.
		glm::vec2 uv; /// Vertex UV coordinates.
	};

	/**
		\fn Mesh
		\brief Constructor.
	*/
	Mesh(void);
	/**
		\fn Mesh
		\brief Constructor that supplies buffers with the given data.
		\param vertices Vertices.
		\param indices Indices.
	*/
	Mesh(const std::vector<Vertex>&, const std::vector<glm::uvec3>&);

	/**
		\fn ~Mesh
		\brief Destructor.

		Calls glDeleteVertexArrays, glDeleteBuffers.
	*/
	virtual ~Mesh(void);

	/**
		\fn draw
		\brief Binds VAO to render the object.
	*/
	virtual void draw(void);

protected:
	/**
		Access specifiers for #objects.
	*/
	enum {VERTEX_ARRAY, VERTEX_BUFFER, ELEMENT_BUFFER, SIZE};

	GLuint objects[SIZE]; ///< OpenGL buffer objects.
	GLsizei count; ///< Number of indices to render.
};

//______________________________________________________________________________
inline Mesh::Vertex::Vertex(const glm::vec3 position, const glm::vec3 normal,
                            const glm::vec2 uv) :
                            position{position}, normal{normal},	uv{uv}
{
}

//______________________________________________________________________________
inline Mesh::~Mesh(void)
{
	glDeleteVertexArrays(1, &objects[VERTEX_ARRAY]);
	glDeleteBuffers(2, &objects[VERTEX_BUFFER]);
}

#endif