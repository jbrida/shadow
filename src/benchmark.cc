// Ján Brída xbrida01 AT stud.fit.vutbr.cz

#include <algorithm>

#include <glm/gtc/matrix_transform.hpp>

#include "benchmark.h"
#include "program.h"
#include "time.h"

//______________________________________________________________________________
void Benchmark::display(Program& p)
{
	fps.feed(calc());
	p.use();
	p.setUniform("PROJ", proj);
	fps.draw();
}

//______________________________________________________________________________
unsigned Benchmark::averageFrameRate(void)
{
	unsigned sum{0}, i{0};

	for (unsigned& f : frameRates) {
		sum += f;
		i++;
	}

	frameRates.clear();

	if (i > 0)
		return sum / i;
	else
		return 0;
}

//______________________________________________________________________________
void Benchmark::Graph::draw(void)
{
	glBindVertexArray(objects[VERTEX_ARRAY]);
	glDrawArrays(GL_POINTS, 0, count);
	glBindVertexArray(0);
}

//______________________________________________________________________________
void Benchmark::Graph::feed(const std::vector<glm::vec2>& vertices)
{
	count = vertices.size();

	glBindVertexArray(objects[VERTEX_ARRAY]);

	if (firstFeed) {
		glBindBuffer(GL_ARRAY_BUFFER, objects[VERTEX_BUFFER]);
		glBufferData(GL_ARRAY_BUFFER, SIZE * sizeof(glm::vec2), &vertices[0],
	               GL_DYNAMIC_DRAW);
		glEnableVertexAttribArray(Mesh::Vertex::POSITION);
		glVertexAttribPointer(Mesh::Vertex::POSITION, sizeof(glm::vec2) /
	                        sizeof(GLfloat), GL_FLOAT, GL_FALSE, 0, nullptr);
		firstFeed = false;
	}
	else
		glBufferSubData(GL_ARRAY_BUFFER, 0, count * sizeof(glm::vec2),
		                &vertices[0]);

	glBindVertexArray(0);
}

//______________________________________________________________________________
std::vector<glm::vec2> Benchmark::calc(void)
{
	acc += Time::getInstance().getDelta();
	if (acc >= 1.0f) {
		acc = 0.0f;

		data.push_back({x * stride, count});
		if (data.size() == Graph::SIZE) {
			data.erase(data.begin());
			std::for_each(data.begin(), data.end(), decX);
		}
		else
			x++;
		frameRates.push_back(count);
		count = 0;
	}
	count++;

	return data;
}

//------------------------------------------------------------------------------
const unsigned Benchmark::stride{10};
const glm::mat4 Benchmark::proj{glm::ortho(0.0f, 190.0f,
                                           0.0f, 300.f,
                                           0.0f, 1.0f)};