// Ján Brída xbrida01 AT stud.fit.vutbr.cz

#include <GLFW/glfw3.h>

#include "input.h"
#include "renderer.h"

//______________________________________________________________________________
void Input::cursorPosCallback(GLFWwindow* window, double posX, double posY)
{
	glfwSetCursorPos(window,
	                 Renderer::getInstance().getWidth() / 2,
	                 Renderer::getInstance().getHeight() / 2);

	static bool firstCall{true};
	if (firstCall)
		firstCall = false;
	else
		getInstance().handleCursorPos(posX, posY);
}

//______________________________________________________________________________
void Input::keyCallback(__attribute__((__unused__)) GLFWwindow*,
                        int key,
	                      __attribute__((__unused__)) int,
	                      int action,
	                      __attribute__((__unused__)) int)
{
	getInstance().handleKey(key, action);
}

//______________________________________________________________________________
void Input::handleCursorPos(const float posX, const float posY)
{
	// Positive rotation is counter-clockwise!
	cursorDelta = {Renderer::getInstance().getWidth() / 2 - posX,
	               Renderer::getInstance().getHeight() / 2 - posY};
}

//______________________________________________________________________________
void Input::handleKey(const int key, const int action)
{
	Key i{OTHER};

	switch (key) {
		case GLFW_KEY_ESCAPE:
			i = ESCAPE;
			break;
		case GLFW_KEY_P:
			i = P;
			break;
		case GLFW_KEY_V:
			i = V;
			break;
		case GLFW_KEY_C:
			i = C;
			break;
		case GLFW_KEY_X:
			i = X;
			break;
		case GLFW_KEY_E:
			i = E;
			break;
		case GLFW_KEY_G:
			i = G;
			break;
		case GLFW_KEY_H:
			i = H;
			break;
		case GLFW_KEY_W:
			i = W;
			break;
		case GLFW_KEY_S:
			i = S;
			break;
		case GLFW_KEY_A:
			i = A;
			break;
		case GLFW_KEY_D:
			i = D;
			break;
		case GLFW_KEY_K:
			i = K;
			break;
		case GLFW_KEY_R:
			i = R;
			break;
		case GLFW_KEY_F1:
			i = F1;
			break;
		case GLFW_KEY_F2:
			i = F2;
		default:
			break;
	}

	if (i) {
		if (action == GLFW_PRESS)
			keysDown[i] = true;
		else if (action == GLFW_RELEASE)
			keysDown[i] = false;
	}
}