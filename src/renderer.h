// Ján Brída xbrida01 AT stud.fit.vutbr.cz
/**
	\file renderer.h
	\brief Implements the #Renderer class.
*/

#ifndef RENDERER_H
#define RENDERER_H

#include <glm/detail/type_vec2.hpp>

#include "parser.h"
#include "manager.h"

class Scene;
class Program;
class Technique;
class Camera;

struct GLFWwindow;

//******************************************************************************
/**
	\class Renderer
	\brief Implements the main application.

	This class represents the core of the application. It runs the main render
	loop, stores all the relevant data, and manipulates with other data types.
*/
class Renderer {
public:
	/**
		\fn getInstance
		\brief Singleton getter.
		\return Singleton of #Renderer.
	*/
	static Renderer& getInstance(void);

	/**
		\fn run
		\brief Publicly callable class from main.cpp.
		\param p Contains program argument information.
	*/
	void run(const Parser);

	/**
		\fn getRes
		\brief Getter for #res.
		\return #res.
	*/
	const glm::ivec2& getRes(void) const { return res; }
	/**
		\fn getWidth
		\brief Returns the x coordinate of #res.
		\return x coordinate of #res.
	*/
	int getWidth(void) const { return res.x; }
	/**
		\fn getHeight
		\brief Returns the y coordinate of #res.
		\return y coordinate of #res.
	*/
	int getHeight(void) const { return res.y; }

private:
	/**
		\enum Clean
		\brief Specifies what to clean out in destructor.
	*/
	enum class Clean {NOTHING, GLFW, WINDOW};
	/**
		\enum State
		\brief Specifies the current state of the application.

		Used to identify different shadow mapping techniques.
	*/
	enum class State {EXIT, PCF, VSM, CSM, XCSM, ESM};

	/**
		\fn Renderer
		\brief Constructor.

		Creates context, sets up OpenGL.
	*/
	Renderer(void);

	/**
		\fn ~Renderer
		\brief Destructor.
	*/
	~Renderer(void);

	/**
		\fn prepare
		\brief Calls #initPrograms, #initCameras, #initTechniques.
	*/
	void prepare(void);
	/**
		\fn initPrograms
		\brief Allocates memory for #programs.
	*/
	void initPrograms(void);
	/**
		\fn initCameras
		\brief Allocates memory for #cameras.
	*/
	void initCameras(void);
	/**
		\fn initTechniques
		\brief Allocates memory for #techniques.
	*/
	void initTechniques(void);
	/**
		\fn loop
		\brief Main render loop.
	*/
	void loop(void);
	/**
		\fn options
		\brief Checks input, and changes #hold, or #conv accordingly.
	*/
	void options(void);
	/**
		\fn select
		\brief Selects #state based on user input.
	*/
	State select(void);
	/**
		\fn showStats
		\brief Shows stats from the previous state on the standard output.
		\param prev Previous state.
	*/
	void showStats(const State);
	/**
		\fn shadowPass
		\brief Renders scene off-screen using a shadow mapping algorithm (stage 1).
		\param s Scene to render.
		\program p Program to be used.
		\program t Responsible FBO.
	*/
	void shadowPass(Scene&, Program&, Technique* const);
	/**
		\fn gaussPass
		\brief Blurs source texture specified by the second parameter.
		\param s Scene to render.
		\param f Contains the filtered texture.
		\param a Contains the intermediate texture.
	*/
	void gaussPass(Scene&, Technique* const, Technique* const);
	/**
		\fn basisPass
		\brief Computes the basis terms for CSM.
		\param s Scene to render.
	*/
	void basisPass(Scene&);
	/**
		\fn objectPass
		\brief Renders scene on-screen.
		\param s Scene to render.
		\program p Program to be used.
		\program t Responsible shadow mapping technique.
	*/
	void objectPass(Scene&, Program&, Technique* const);
	/**
		\fn casterView
		\brief Render scene from the light caster perspective.
		\param s Scene to render.
	*/
	void casterView(Scene&);
	/**
		\fn plotPass
		\brief Plots the FPS graph using data from the #Benchmark singleton.
	*/
	void plotPass(void);
	/**
		\fn viewport
		\brief Calls glViewport.
		\param res Resolution.
		\param origin Origin.
	*/
	static void viewport(const glm::ivec2, const glm::ivec2 = {});
	/**
		\fn clear
		\brief Clears color/depth buffers.
		\param both If to clear both buffers (color/depth, if false then it clears
		only the color buffer).
	*/
	static void clear(const bool = true);
	/**
		\fn drawBuffers
		\brief Sets glDrawBuffers accordingly.
		\param n Limit to this value. Starts from GL_COLOR_ATTACHMENT0.
	*/
	static void drawBuffers(const unsigned = 0);
	/**
		\fn scissorDepth
		\brief Scissors the depth buffer bit to be able to render graph,
		and the caster's view.
		\param res Resolution.
		\param origin Origin.
	*/
	static void scissorDepth(const glm::ivec2, const glm::ivec2 = {});

	const glm::ivec2 res{800, 600}; ///< Display resolution.
	glm::ivec2 mapRes{1024}; ///< Depth map(s) size.
	GLFWwindow* window; ///< GLFW window handle.
	Clean clean{Clean::NOTHING}; ///< Cleaning specifier.
	Manager<Program> programs; ///< Programs.
	Manager<Camera> cameras; ///< Cameras.
	Manager<Technique> techniques; ///< Techniques.
	bool hold{false}, ///< If to hold camera position.
	     conv{false}; ///< If to use Gaussian blur.
};

//______________________________________________________________________________
inline Renderer& Renderer::getInstance(void)
{
	static Renderer singleton;
	return singleton;
}

//______________________________________________________________________________
inline void Renderer::run(const Parser p)
{
	if (p.get())
		mapRes = glm::ivec2{p.get()};
	prepare();
	loop();
}

//______________________________________________________________________________
inline void Renderer::prepare(void)
{
	initPrograms();
	initCameras();
	initTechniques();
}

#endif