// Ján Brída xbrida01 AT stud.fit.vutbr.cz
/**
	\file parser.h
	\brief Simple argument parser definition here.
*/

#ifndef PARSER_H
#define PARSER_H

#include <stdexcept>

//******************************************************************************
/**
	\class Parser
	\brief Parses program arguments on start up.
*/
class Parser {
public:
	/**
		\fn Parser
		\brief Constructor.

		Parses arguments.
		\param argc Argument count.
		\param argv Argument vector.
	*/
	Parser(const int, const char** const);

	/**
		\fn ~Parser
		\brief Destructor.
	*/
	~Parser(void) {}

	/**
		\fn get
		\brief Returns #v.
		\return #v.
	*/
	int get(void) const { return v; }

private:
	/**
		\fn isPowerOfTwo
		\brief Checks if the parsed value is a power of two.
		\param x Initial value.
	*/
	static bool isPowerOfTwo(int);

	int v{0}; ///< The parsed value.
};

//______________________________________________________________________________
inline Parser::Parser(const int i, const char** const a)
{
	if (i > 2)
		throw std::runtime_error{"Parser::Parser: illegal number of arguments"};

	if (i != 1) {
		v = atoi(a[1]);
		if (!isPowerOfTwo(v))
			throw std::runtime_error{"Parser::Parser: invalid supplied argument"};
	}
}

//______________________________________________________________________________
inline bool Parser::isPowerOfTwo(int x)
{
	while (x % 2 == 0 && x > 1)
		x /= 2;
	return x == 1;
}

#endif