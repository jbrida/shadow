// Ján Brída xbrida01 AT stud.fit.vutbr.cz
/**
	\file program.h
	\brief Implements #Program.
*/

#ifndef SHADER_H
#define SHADER_H

#include <string>
#include <vector>
#include <list>
#include <unordered_map>
#include <stdexcept>

#include <GL/glew.h>
#include <glm/detail/type_vec2.hpp>
#include <glm/detail/type_vec3.hpp>
#include <glm/detail/type_mat3x3.hpp>
#include <glm/detail/type_mat4x4.hpp>

namespace logger {
	enum {SIZE = 512};
}

//******************************************************************************
/**
	\class Program
	\brief Encapsulates OpenGL shader program.
*/
class Program {
public:
	/**
		\fn Program
		\brief Constructor.

		Compiles the shader programs, and links all together.
		\param vert Filename of vertex shader.
		\param frag Filename of fragment shader.
	*/
	Program(const std::string, const std::string);

	/**
		\fn ~Program
		\brief Destructor.

		Calls glDeleteProgram.
	*/
	~Program(void) { glDeleteProgram(id); }

	/**
		\fn getId
		\brief Getter for #id.
		\return #id.
	*/
	GLuint getId(void) const { return id; }

	/**
		\fn use
		\brief Uses the shader program.
	*/
	void use(void);
	/**
		\fn Program::setUniform(const std::string, const GLint)
		\brief
		\param name Uniform name.
		\param val Value.
	*/
	void setUniform(const std::string, const GLint);
	/**
		\fn Program::setUniform(const std::string, const std::vector<GLint>&)
		\brief
		\param name Uniform name.
		\param vals An array of values.
	*/
	void setUniform(const std::string, const std::vector<GLint>&);
	/**
		\fn Program::setUniform(const std::string, const glm::ivec2&)
		\brief
		\param name Uniform name.
		\param vec 2D integer vector.
	*/
	void setUniform(const std::string, const glm::ivec2&);
	/**
		\fn Program::setUniform(const std::string, const glm::vec2&)
		\brief
		\param name Uniform name.
		\param vec 2D float vector.
	*/
	void setUniform(const std::string, const glm::vec2&);
	/**
		\fn Program::setUniform(const std::string, const glm::vec3&)
		\brief
		\param name Uniform name.
		\param vec 3D float vector.
	*/
	void setUniform(const std::string, const glm::vec3&);
	/**
		\fn Program::setUniform(const std::string, const glm::mat3&)
		\brief
		\param name Uniform name.
		\param mat Matrix 3x3.
	*/
	void setUniform(const std::string, const glm::mat3&);
	/**
		\fn Program::setUniform(const std::string, const glm::mat4&)
		\brief
		\param name Uniform name.
		\param mat Matrix 4x4.
	*/
	void setUniform(const std::string, const glm::mat4&);

	/**
		\fn getUsed
		\brief Static getter for #used #Program.
		\return An instance of #Program, the currently used shader program.
	*/
	static Program& getUsed(void);

private:
	/**
		\class Shader
		\brief Encapsulates OpenGL shader object.
	*/
	class Shader {
	public:
		/**
			\fn Shader
			\brief Constructor.

			Creates a shader object.
			\param type Shader type.
			\param program Parent #Program reference.
			\param source Source code of the shader program.
		*/
		Shader(const GLenum, const Program&, const GLchar* const);

		/**
			\fn ~Shader
			\brief Destructor.

			Calls glDetachShader, glDeleteShader.
		*/
		~Shader(void);

		/**
			\fn attach
			\brief Attaches the shader to the parent #program.
		*/
		void attach(void) { glAttachShader(program.getId(), id); }

	private:
		/**
			\fn errorCheck
			\brief Checks for errors after compilation. Throws an exception if found.
		*/
		void errorCheck(void) const;

		const GLuint id; ///< OpenGL shader object name.
		const Program& program; ///< Parent #Program.
	};

	/**
		\fn uniformLocation
		\brief Gets the uniform location based on the single parameter.
		\param name Uniform name.
		\return Uniform location.
	*/
	GLint uniformLocation(const std::string&);
	/**
		\fn link
		\brief Links shaders in the list.
		\param shaders List of shaders to be linked.
	*/
	void link(std::list<Shader>);
	/**
		\fn errorCheck
		\brief Checks for errors after linking. Throws an exception if found.
	*/
	void errorCheck(void) const;
	/**
		\fn loadSource
		\brief Static method to load source code from file.
		\param filename Filename of the shader.
	*/
	static const GLchar* loadSource(const std::string&);

	const GLuint id{glCreateProgram()}; ///< OpenGL program object name.
	std::unordered_map<std::string, GLint> uniforms; ///< Uniform map.

	static Program* used; ///< Currently used shader program.
	static const std::string shaderDir; ///< Shader directory prefix.
};

//______________________________________________________________________________
inline void Program::use(void)
{
	static GLuint used{0};
	if (id != used) {
		glUseProgram(id);
		used = id;
		Program::used = this;
	}
}

//______________________________________________________________________________
inline void Program::setUniform(const std::string name, const GLint val)
{
	glUniform1i(uniformLocation(name), val);
}

//______________________________________________________________________________
inline void Program::setUniform(const std::string name,
                                const std::vector<GLint>& vals)
{
	glUniform1iv(uniformLocation(name), vals.size(), &vals[0]);
}

//______________________________________________________________________________
inline void Program::setUniform(const std::string name, const glm::ivec2& vec)
{
	glUniform2iv(uniformLocation(name), 1, &vec[0]);
}

//______________________________________________________________________________
inline void Program::setUniform(const std::string name, const glm::vec2& vec)
{
	glUniform2fv(uniformLocation(name), 1, &vec[0]);
}

//______________________________________________________________________________
inline void Program::setUniform(const std::string name, const glm::vec3& vec)
{
	glUniform3fv(uniformLocation(name), 1, &vec[0]);
}

//______________________________________________________________________________
inline void Program::setUniform(const std::string name, const glm::mat3& mat)
{
	glUniformMatrix3fv(uniformLocation(name), 1, GL_FALSE, &mat[0][0]);
}

//______________________________________________________________________________
inline void Program::setUniform(const std::string name, const glm::mat4& mat)
{
	glUniformMatrix4fv(uniformLocation(name), 1, GL_FALSE, &mat[0][0]);
}

//______________________________________________________________________________
inline Program::Shader::~Shader(void)
{
	glDetachShader(program.getId(), id);
	glDeleteShader(id);
}

//______________________________________________________________________________
inline GLint Program::uniformLocation(const std::string& name)
{
	if (uniforms.find(name) == uniforms.end()) {
		uniforms[name] = glGetUniformLocation(id, name.c_str());
		if (uniforms[name] == -1)
			throw std::runtime_error{"glGetUniformLocation: invalid location"};
	}
	return uniforms[name];
}

//______________________________________________________________________________
inline Program& Program::getUsed(void)
{
	if (!used)
		throw std::runtime_error{"Program::getUsed: no program used"};
	return *used;
}

#endif