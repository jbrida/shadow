// Ján Brída xbrida01 AT stud.fit.vutbr.cz
/**
	\file input.h
	\brief Here is implemented mouse/keyboard input handling.
*/

#ifndef INPUT_H
#define INPUT_H

struct GLFWwindow;

#include <glm/detail/type_vec2.hpp>

//******************************************************************************
/**
	\class Input
	\brief Handles mouse/keyboard input.
*/
class Input {
public:
	/**
		\enum Key
		\brief This type is used as an index into #keysDown.
	*/
	enum Key {OTHER, ESCAPE, P, V, C, X, E, G, H, W, S, A, D, K, R, F1, F2, SIZE};

	/**
		\fn getInstance
		\brief Returns singleton of #Input.
		\return Singleton of #Input.
	*/
	static Input& getInstance(void);

	/**
		\fn cursorPosCallback
		\brief GLFW cursor position callback.
		\param window
		\param posX
		\param posY
	*/
	static void cursorPosCallback(GLFWwindow*, double, double);
	/**
		\fn keyCallback
		\brief GLFW key callback.
		\param x Not used.
		\param key Key.
		\param x Not used.
		\param action Action.
		\param x Not used.
	*/
	static void keyCallback(GLFWwindow*, int, int, int, int);

	/**
		\fn handleCursorPos
		\brief Handles cursor position information.
		\param posX x coordinate.
		\param posY y coordinate.
	*/
	void handleCursorPos(const float, const float);
	/**
		\fn handleKey
		\brief Handles key information.
		\param key Key.
		\param action Action.
	*/
	void handleKey(const int, const int);

	/**
		\fn getCursorDelta
		\brief Getter for #cursorDelta.
		\return #cursorDelta.
	*/
	glm::vec2 getCursorDelta(void);
	/**
		\fn isKeyDown
		\brief Tests if the identified key is down.
		\param i Index to #keysDown.
	*/
	bool isKeyDown(const Key) const;

private:
	glm::vec2 cursorDelta{0.0f}; ///< Cursor position delta.
	bool keysDown[SIZE]{false}; ///< Array of pressed/not pressed keys.
};

//______________________________________________________________________________
inline Input& Input::getInstance(void)
{
	static Input singleton;
	return singleton;
}

//______________________________________________________________________________
inline glm::vec2 Input::getCursorDelta(void)
{
	const glm::vec2 cursorDelta{this->cursorDelta};
	this->cursorDelta = {0.0f, 0.0f};
	return cursorDelta;
}

//______________________________________________________________________________
inline bool Input::isKeyDown(const Key i) const
{
	return (i && i < SIZE) ? keysDown[i] : false;
}

#endif