#include <stdexcept>
#include <vector>

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include "scene.h"
#include "camera.h"
#include "technique.h"

//______________________________________________________________________________
Scene::Scene(const std::string path)
{
	Assimp::Importer imp;
	const aiScene* const scene{imp.ReadFile((objDir + path).c_str(),
	                           aiProcess_Triangulate | aiProcess_GenNormals)};
	if (!scene || scene->mFlags == AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode)
		throw std::runtime_error{"Assimp::Importer::ReadFile: " +
	                           std::string{imp.GetErrorString()}};

	processNode(scene->mRootNode, *scene);
}

//______________________________________________________________________________
Scene::~Scene(void)
{
	for (Mesh* m : meshes)
		delete m;
}

//______________________________________________________________________________
void Scene::processNode(aiNode* const node, const aiScene& scene)
{
	for (unsigned i{0}; i < node->mNumMeshes; i++) {
		processMesh(*scene.mMeshes[node->mMeshes[i]]);
		nodes.push_back({*meshes.back(),
		                 processMaterial(*scene.mMaterials[scene.mMeshes[node->
		                                 mMeshes[i]]->mMaterialIndex])});
	}

	for (unsigned i{0}; i < node->mNumChildren; i++)
		processNode(node->mChildren[i], scene);
}

//______________________________________________________________________________
void Scene::processMesh(const aiMesh& mesh)
{
	if (!mesh.HasPositions() || !mesh.HasNormals())
		throw std::runtime_error{"Scene::processMesh: missing data"};

	std::vector<Mesh::Vertex> vertices;
	std::vector<glm::uvec3> indices;

	for (unsigned i{0}; i < mesh.mNumVertices; i++) {
		glm::vec2 uv;
		if (mesh.HasTextureCoords(0))
			uv = {mesh.mTextureCoords[0][i].x, mesh.mTextureCoords[0][i].y};
		vertices.push_back({{mesh.mVertices[i].x,
		                     mesh.mVertices[i].y,
		                     mesh.mVertices[i].z},
		                    {mesh.mNormals[i].x,
		                     mesh.mNormals[i].y,
		                     mesh.mNormals[i].z},
		                    uv});
	}

	for (unsigned i{0}; i < mesh.mNumFaces; i++) {
		if (mesh.mFaces[i].mNumIndices != 3)
			throw std::runtime_error{"Scene::processMesh: illegal face structure"};
		indices.push_back({mesh.mFaces[i].mIndices[0],
		                   mesh.mFaces[i].mIndices[1],
		                   mesh.mFaces[i].mIndices[2]});
	}

	meshes.push_back(new Mesh{vertices, indices});
}

//______________________________________________________________________________
Texture* Scene::processMaterial(const aiMaterial& material)
{
	if (!material.GetTextureCount(aiTextureType_DIFFUSE))
		return nullptr;

	aiString filename;
	if (material.GetTexture(aiTextureType_DIFFUSE, 0, &filename) != AI_SUCCESS)
		throw std::runtime_error{"aiMaterial::GetTexture: error value returned"};

	if (!textures.isPresent(filename.data)) {
		textures.add(filename.data, new Texture);
		textures[filename.data].load(Texture::RGB, filename.data);
	}

	return &textures[filename.data];
}

//______________________________________________________________________________
void Scene::renderAll(Program& program, const Camera& camera,
                      const std::list<Technique*> techniques,
                      const bool uvMappingOn)
{
	program.use();

	for (Technique* t : techniques)
		t->prologue();

	for (Node& n : nodes) {
		for (Technique* t : techniques)
			t->apply(n.getModel());
		program.setUniform("MVP", camera.computeMvp(n.getModel()));
		if (uvMappingOn)
			n.mapUv(texUnit);
		n.render();
	}
}

//------------------------------------------------------------------------------
const std::string Scene::objDir{"objs/"};