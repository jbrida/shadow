// Ján Brída xbrida01 AT stud.fit.vutbr.cz
/**
	\file scene.h
	\brief Implements #Scene, and #Scene::Node.
*/

#ifndef SCENE_H
#define SCENE_H

#include <string>
#include <list>

#include <glm/detail/type_mat4x4.hpp>

#include "mesh.h"
#include "texture.h"
#include "manager.h"
#include "program.h"

struct aiNode;
struct aiScene;
struct aiMesh;
struct aiMaterial;
class Camera;
class Technique;

//******************************************************************************
/**
	\class Scene
	\brief This class loads (using Assimp), and renders all meshes associated with
	the scene.

	It calls list of techniques during a #renderAll loop.
*/
class Scene {
public:
	/**
		\fn Scene
		\brief Constructor.
		\param filename Filename of the 3D model to be loaded, and supplied into
		OpenGL.
	*/
	Scene(const std::string);

	/**
		\fn Scene
		\brief Destructor.
	*/
	~Scene(void);

	/**
		\fn setTexUnit
		\brief Setter for #texUnit.
		\param texUnit New #texUnit.
	*/
	void setTexUnit(const GLint texUnit) { this->texUnit = texUnit; }

	/**
		\fn renderAll
		\brief Calls #Scene::Node::render() function for each mesh in #meshes.
		\param program Program to be used for rendering.
		\param camera Observing camera used for transformations.
		\param techniques List of applied techniques.
		\param uvMappingOn Sets UV mapping on/off.
	*/
	void renderAll(Program&, const Camera&, const std::list<Technique*> = {},
	               const bool = false);

private:
	/**
		\class Node
		\brief #Node is a single element (mesh) in #Scene. It also contains
		information about #model, and material.
	*/
	class Node {
	public:
		/**
			\fn Node
			\brief Constructor.
			\param mesh #Mesh to be used.
			\param texture Texture to be used.
		*/
		Node(Mesh&, Texture* const);

		/**
			\fn ~Node
			\brief Destructor.
		*/
		~Node(void) {}

		/**
			\fn getModel
			\brief Getter for #model.
			\return #model.
		*/
		const glm::mat4& getModel(void) const { return model; }

		/**
			\fn setModel
			\brief Setter for #model.
			\param model New #model.
		*/
		void setModel(const glm::mat4& model) { this->model = model; }

		/**
			\fn mapUv
			\brief Maps UV coordinates by setting sampler2D uniform,
			and allows it to be used by setting the MAP_UV uniform.
			\param i Used texture unit.
		*/
		void mapUv(const GLint);
		/**
			\fn render
			\brief Calls #Mesh::draw on #mesh.
		*/
		void render(void) { mesh.draw(); }

	private:
		Mesh& mesh; ///< Used #Mesh.
		Texture* texture; ///< Used texture.
		glm::mat4 model; ///< Model matrix.
	};

	/**
		\fn processNode
		\brief Recursive function that processes all nodes
		of the 3D model scene.
		\param node Node.
		\param scene Scene.
	*/
	void processNode(aiNode* const, const aiScene&);
	/**
		\fn processMesh
		\brief Processes the mesh.
		\param mesh
	*/
	void processMesh(const aiMesh&);
	/**
		\fn processMaterial
		\brief Processes the material.
		\param material Material.
		\return Texture pointer corresponding with the material.
	*/
	Texture* processMaterial(const aiMaterial&);

	std::list<Mesh*> meshes; ///< Meshes.
	Manager<Texture> textures; ///< Textures (optimized).
	std::list<Node> nodes; ///< Nodes.
	GLint texUnit{0}; ///< Used texture unit for covering object with images.

	static const std::string objDir; ///< OBJ format files directory prefix.
};

//______________________________________________________________________________
inline Scene::Node::Node(Mesh& mesh, Texture* const texture) :
                         mesh{mesh}, texture{texture}
{
}

//______________________________________________________________________________
inline void Scene::Node::mapUv(const GLint i)
{
	if (texture) {
		Program::getUsed().setUniform("MAP_UV", static_cast<GLint>(1));
		Program::getUsed().setUniform("DIFFUSE", i);
		Texture::bind(texture->getId(), i);
	}
	else
		Program::getUsed().setUniform("MAP_UV", static_cast<GLint>(0));
}

#endif