// Ján Brída xbrida01 AT stud.fit.vutbr.cz
/**
	\file depth_map.h
	\brief This file implements shadow map using the #DepthMap class.

	#DepthMap can be one of many different variations of a shadow map, making it
	easy to use with different techniques.
*/

#ifndef DEPTH_MAP_H
#define DEPTH_MAP_H

#include "technique.h"
#include "offscreen.h"
#include "camera.h"
#include "program.h"

//******************************************************************************
/**
	\class DepthMap
	\brief #DepthMap derives from both Technique (to implement uniform handling),
	and Offscreen (for framebuffer usage).

	It is used to implement shadow map handling in shader programs: to write,
	and read from it.
*/
class DepthMap : public Technique, public Offscreen {
public:
	/**
		\fn DepthMap
		\brief Constructor.
		\param gbuffer New framebuffer object.
		\param format Format of the texture attached to the framebuffer.
		\param caster Light caster.
	*/
	DepthMap(Framebuffer* const, const Texture::Format, const Camera&);

	/**
		\fn ~DepthMap
		\brief Destructor.
	*/
	~DepthMap(void) {}

	/**
		\fn prologue
		\brief Binds the texture(s).
	*/
	void prologue(void);
	/**
		\fn apply
		\brief Applies matrix transformation.
	*/
	void apply(const glm::mat4&);

private:
	const Camera& caster; ///< Light caster reference.
};

//______________________________________________________________________________
inline DepthMap::DepthMap(Framebuffer* const gbuffer,
                          const Texture::Format format, const Camera& caster) :
                          Technique{}, Offscreen{gbuffer}, caster{caster}
{
	gbuffer->attach(format);
	texUnit = 1;
}

//______________________________________________________________________________
inline void DepthMap::prologue(void)
{
	Program::getUsed().setUniform("DEPTH_MAP", texUnits());
	gbuffer->read(texUnit);
}

//______________________________________________________________________________
inline void DepthMap::apply(const glm::mat4& model)
{
	Program::getUsed().setUniform("CASTER_MVP", caster.computeMvp(model));
}

#endif