// Ján Brída xbrida01 AT stud.fit.vutbr.cz
/**
	\file phong.h
	\brief This file implements the %Phong shading model using the class #Phong.
*/

#ifndef PHONG_H
#define PHONG_H

#include "technique.h"
#include "camera.h"
#include "program.h"

//******************************************************************************
/**
	\class Phong
	\brief Implements the %Phong shading model.
*/
class Phong : public Technique {
public:
	/**
		\fn Phong.
		\brief Constructor.
		\param caster Caster camera.
		\param viewer Viewer camera.
	*/
	Phong(const Camera&, const Camera&);

	/**
		\fn ~Phong
		\brief Destructor.
	*/
	~Phong(void) {}

	/**
		\fn prologue
		\brief Used to set the CASTER_EYE uniform.
	*/
	void prologue(void);
	/**
		\fn apply
		\brief Sets the VIEWER, and NORMAL uniforms.
		\param model Model.
	*/
	void apply(const glm::mat4&);

private:
	const Camera& caster, ///< Light caster.
	              viewer; ///< Observer.
};

//______________________________________________________________________________
inline Phong::Phong(const Camera& caster, const Camera& viewer) :
                    Technique{}, caster{caster}, viewer{viewer}
{
}

//______________________________________________________________________________
inline void Phong::prologue(void)
{
	Program::getUsed().setUniform("CASTER_EYE", glm::vec3{viewer.getView() *
	                              glm::vec4{caster.getEye(), 1.0}});
}

//______________________________________________________________________________
inline void Phong::apply(const glm::mat4& model)
{
	Program::getUsed().setUniform("VIEWER", viewer.getView() * model);
	Program::getUsed().setUniform("NORMAL", viewer.computeNormal(model));
}

#endif