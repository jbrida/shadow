// Ján Brída xbrida01 AT stud.fit.vutbr.cz

#include <stdexcept>

#include <GL/glew.h>
#include <SOIL.h>

#include "texture.h"

//______________________________________________________________________________
void Texture::load(const Format format, const std::string filename)
{
	this->format = format;

	Image image{GL_RGB, GL_RGB, GL_UNSIGNED_BYTE};
	switch (format) {
		case R32F:
			image = {GL_R32F, GL_RED, GL_FLOAT};
			break;
		case RG32F:
			image = {GL_RG32F, GL_RG, GL_FLOAT};
			break;
		case RGBA16F:
			image = {GL_RGBA16F, GL_RGBA, GL_FLOAT};
		default:
			break;
	}

	bind(id);
	if (filename != "") {
		int channels;
		image.data = SOIL_load_image(std::string{imageDir + filename}.c_str(),
		                             &res.x, &res.y, &channels, SOIL_LOAD_RGB);
		if (!image.data)
			throw std::runtime_error{"SOIL_load_image: cannot load from file \"" +
		                           filename + "\""};
	}
	else {
		if (!res.x || !res.y)
			throw std::runtime_error{"Texture::load: zero resolution attribute"};

		wrap(GL_CLAMP_TO_EDGE);
	}

	filter(GL_LINEAR_MIPMAP_LINEAR);
	anisotropy();
	glTexImage2D(GL_TEXTURE_2D, 0, image.internal, res.x, res.y, 0, image.format,
	             image.type, image.data);

	if (image.data) {
		SOIL_free_image_data(image.data);
		genMip();
	}

	bind();
}

//------------------------------------------------------------------------------
const std::string Texture::imageDir{"images/"};