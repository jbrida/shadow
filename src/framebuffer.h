// Ján Brída xbrida01 AT stud.fit.vutbr.cz
/**
	\file framebuffer.h
	\brief Here is defined OpenGL framebuffer object encapsulation.
*/

#ifndef FRAMEBUFFER_H
#define FRAMEBUFFER_H

#include <vector>

#include "texture.h"

//******************************************************************************
/**
	\class Framebuffer
	\brief Encapsulates OpenGL framebuffer object.
*/
class Framebuffer {
public:
	/**
		\fn Framebuffer
		\brief Constructor.
		\param res Texture(s), and framebuffer resolution.
		\param n Number of textures to be generated, and attached.
	*/
	Framebuffer(const glm::ivec2&, const unsigned = 1);

	/**
		\fn ~Framebuffer
		\brief Destructor.

		Calls glDeleteFramebuffers.
	*/
	~Framebuffer(void) { glDeleteFramebuffers(1, &id); }

	/**
		\fn getId
		\brief Getter for #id.
		\return #id.
	*/
	GLuint getId(void) const { return id; }
	/**
		\fn getSize
		\brief Returns the size of #textures.
		\return Size of #textures.
	*/
	unsigned getSize(void) const { return textures.size(); }
	/**
		\fn getTexture
		\brief Returns the texture in #textures based on the single parameter.
		\param i Index to #textures.
		\return Specified texture.
	*/
	Texture& getTexture(const unsigned i = 0) { return textures[i]; }

	/**
		\fn bind
		\brief Binds the FBO.
		\param id FBO name.
	*/
	static void bind(const GLuint = 0);
	/**
		\fn read
		\brief Binds textures.
		\param Starting texture unit.
	*/
	void read(unsigned);
	/**
		\fn attach
		\brief Used to attach texture(s) to FBO using the supplied texture format.
		\param format Specifies the OpenGL format of texture.
	*/
	void attach(const Texture::Format);

private:
	/**
		\class Renderbuffer
		\brief Simple encapsulation of OpenGL renderbuffer object.
	*/
	class Renderbuffer {
	public:
		/**
			\fn Renderbuffer
			\brief Constructor.
		*/
		Renderbuffer(void);

		/**
			\fn ~Renderbuffer
			\brief Destructor.

			Calls glDeleteRenderbuffers.
		*/
		~Renderbuffer(void) { glDeleteRenderbuffers(1, &id); }

		/**
			\fn getId
			\brief Getter for #id.
			\return #id.
		*/
		GLuint getId(void) { return id; }

		/**
			\fn storage
			\brief Sets renderbuffer storage.
			\param width The resolution width.
			\param height The resolution height.
		*/
		void storage(const GLsizei, const GLsizei);

	private:
		GLuint id; /// OpenGL renderbuffer object name.
	};

	GLuint id; ///< OpenGL FBO name.
	std::vector<Texture> textures; ///< Vector of attached textures.
	Renderbuffer renderbuffer; ///< Renderbuffer serving as a depth buffer.
};

//______________________________________________________________________________
inline void Framebuffer::bind(const GLuint id)
{
	static GLuint bound{0};
	if (id != bound) {
		glBindFramebuffer(GL_FRAMEBUFFER, id);
		bound = id;
	}
}

//______________________________________________________________________________
inline Framebuffer::Renderbuffer::Renderbuffer(void)
{
	glGenRenderbuffers(1, &id);
	if (!id)
		throw std::runtime_error{"glGenRenderbuffers: unable to create object"};
}

//______________________________________________________________________________
inline void Framebuffer::Renderbuffer::storage(const GLsizei width,
                                               const GLsizei height)
{
	glBindRenderbuffer(GL_RENDERBUFFER, id);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
	glBindRenderbuffer(GL_RENDERBUFFER, 0);
}

#endif