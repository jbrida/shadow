// Ján Brída xbrida01 AT stud.fit.vutbr.cz
/**
	\mainpage Doxygen Documentation of the Shadow Filtering %Benchmark Application

	\section sec_desc Description
	This HTML document describes implementation details of the Shadow application
	used in Shadow Mapping: Shadow filtering in OpenGL bachelor's thesis by Ján
	Brída (xbrida01@stud.fit.vutbr.cz). This application implements, and helps
	asses performance qualities of Percentage-Closer Filtering, Variance Shadow
	Maps, Convolution Shadow Maps, Experimental Convolution Shadow Maps, and
	Exponential Shadow Maps. Please read README for instructions.
*/

#include <exception>
#include <iostream>

#include "renderer.h"

//______________________________________________________________________________
int main(const int argc, const char** const argv)
{
	try {
		Renderer::getInstance().run({argc, argv});
	}
	catch (const std::exception& e) {
		std::cerr << "THROWN: " << e.what() << std::endl;
	}

	return 0;
}