# Ján Brída xbrida01 AT stud.fit.vutbr.cz

# WARNING:
# Please make sure GLFW version is 3.0.4! Newer versions created some input
# problems. Thank you for understanding.

CXX = g++
CXXFLAGS = -std=c++11 \
           -pedantic -pedantic-errors \
           -Wpedantic -Wall -Wextra -Werror -Wfatal-errors \
           -O3
CPPFLAGS = -DGLEW_STATIC \
           -DGLM_FORCE_CXX11 -DGLM_SWIZZLE -DGLM_SWIZZLE_XYZW \
           -DGLM_FORCE_RADIANS \
           -ID:\SDKs\glew-1.11.0\include \
           -ID:\SDKs\glfw-3.0.4.bin.WIN32\include \
           -ID:\SDKs\glm \
           -ID:\SDKs\SOIL\include \
           -ID:\SDKs\assimp-3.1.1\include
LDFLAGS = -LD:\SDKs\glew-1.11.0\lib \
          -LD:\SDKs\glfw-3.0.4.bin.WIN32\lib-mingw \
          -LD:\SDKs\SOIL\lib \
          -LD:\SDKs\assimp-3.1.1\lib
LDLIBS = -lglew32 \
         -lglfw3 \
         -lsoil \
         -lopengl32 -lgdi32 \
				 -lassimp

TARGET = shadow
SRC = src
OBJS = main.o renderer.o input.o program.o mesh.o texture.o camera.o \
       spectator.o framebuffer.o scene.o benchmark.o

all: $(TARGET)

run: $(TARGET) clean
	shadow

doxy: Doxyfile
	doxygen

$(TARGET): $(OBJS)
	$(CXX) $(CPPFLAGS) $(LDFLAGS) $(CXXFLAGS) -o $(TARGET) $(OBJS) $(LDLIBS)

main.o: $(SRC)/main.cc $(SRC)/renderer.h
	$(CXX) $(CPPFLAGS) $(LDFLAGS) $(CXXFLAGS) -c $(SRC)/main.cc $(LDLIBS)

renderer.o: $(SRC)/renderer.cc $(SRC)/renderer.h $(SRC)/manager.h \
            $(SRC)/input.h $(SRC)/program.h $(SRC)/camera.h $(SRC)/phong.h \
            $(SRC)/depth_map.h $(SRC)/filter.h $(SRC)/scene.h $(SRC)/time.h \
            $(SRC)/benchmark.h
	$(CXX) $(CPPFLAGS) $(LDFLAGS) $(CXXFLAGS) -c $(SRC)/renderer.cc $(LDLIBS)

input.o: $(SRC)/input.cc $(SRC)/input.h $(SRC)/renderer.h
	$(CXX) $(CPPFLAGS) $(LDFLAGS) $(CXXFLAGS) -c $(SRC)/input.cc $(LDLIBS)

program.o: $(SRC)/program.cc $(SRC)/program.h
	$(CXX) $(CPPFLAGS) $(LDFLAGS) $(CXXFLAGS) -c $(SRC)/program.cc $(LDLIBS)

mesh.o: $(SRC)/mesh.cc $(SRC)/mesh.h
	$(CXX) $(CPPFLAGS) $(LDFLAGS) $(CXXFLAGS) -c $(SRC)/mesh.cc $(LDLIBS)

texture.o: $(SRC)/texture.cc $(SRC)/texture.h
	$(CXX) $(CPPFLAGS) $(LDFLAGS) $(CXXFLAGS) -c $(SRC)/texture.cc $(LDLIBS)

camera.o: $(SRC)/camera.cc $(SRC)/camera.h $(SRC)/input.h $(SRC)/time.h
	$(CXX) $(CPPFLAGS) $(LDFLAGS) $(CXXFLAGS) -c $(SRC)/camera.cc $(LDLIBS)

spectator.o: $(SRC)/spectator.cc $(SRC)/spectator.h $(SRC)/time.h
	$(CXX) $(CPPFLAGS) $(LDFLAGS) $(CXXFLAGS) -c $(SRC)/spectator.cc $(LDLIBS)

framebuffer.o: $(SRC)/framebuffer.cc $(SRC)/framebuffer.h $(SRC)/texture.h
	$(CXX) $(CPPFLAGS) $(LDFLAGS) $(CXXFLAGS) -c $(SRC)/framebuffer.cc $(LDLIBS)

scene.o: $(SRC)/scene.cc $(SRC)/scene.h $(SRC)/camera.h $(SRC)/technique.h
	$(CXX) $(CPPFLAGS) $(LDFLAGS) $(CXXFLAGS) -c $(SRC)/scene.cc $(LDLIBS)

benchmark.o: $(SRC)/benchmark.cc $(SRC)/benchmark.h $(SRC)/program.h \
             $(SRC)/time.h
	$(CXX) $(CPPFLAGS) $(LDFLAGS) $(CXXFLAGS) -c $(SRC)/benchmark.cc $(LDLIBS)

.PHONY: clean
clean:
	del $(OBJS)