// Ján Brída xbrida01 AT stud.fit.vutbr.cz

#version 420

#define M 16
#define PI 3.1415926535897932384626433832795

in Vert {
	vec2 uv;
} vert;

in Shading {
	vec3 position, normal;
} shading;

in Shadow {
	vec4 p;
} shadow;

out vec4 fragColor;

uniform bool MAP_UV;
uniform sampler2D DIFFUSE, DEPTH_MAP[M / 2];
uniform vec3 CASTER_EYE;

//______________________________________________________________________________
bool inLightSpace(const vec3 p)
{
	return p.x <= 1.0 && p.x >= -1.0 && p.y <= 1.0 && p.y >= -1.0;
}

//______________________________________________________________________________
// Get rid off the Gibbs phenomenon.
float attenuate(const float v, const uint k)
{
	const float alpha = 1.0;
	return v * exp(-alpha * pow(k / M, 2));
}

//______________________________________________________________________________
float csm(const float d, const vec2 uv)
{
	float sum = 0.5;

	for (uint i = 0, k = 1; i < M / 2; i++, k += 2) {
		const vec2 ck = vec2(PI * (2 * k - 1), PI * (2 * (k + 1) - 1));
		const vec4 a = vec4((2.0 / ck.x) * cos(ck.x * d),
		                    (-2.0 / ck.x) * sin(ck.x * d),
		                    (2.0 / ck.y) * cos(ck.y * d),
		                    (-2.0 / ck.y) * sin(ck.y * d)),
		           b = texture(DEPTH_MAP[i], uv);
		sum += attenuate(a.x * b.x, k) +
		       attenuate(a.y * b.y, k) +
		       attenuate(a.z * b.z, k + 1) +
		       attenuate(a.w * b.w, k + 1);
	}

	const float scaling = 2.0;
	return clamp(scaling * sum, 0.0, 1.0);
}

//______________________________________________________________________________
float visibility(const bool averted)
{
	float result = 0.2;

	const vec3 p = shadow.p.xyz / shadow.p.w;
	if (!averted && inLightSpace(p) && shadow.p.w > 0.0)
		result = csm(p.z * 0.5 + 0.5, p.xy * 0.5 + 0.5);

	return result;
}

//______________________________________________________________________________
vec4 phong(void)
{
	vec3 diffuse = vec3(0.5),
	     specular = vec3(0.5);

	const vec3 n = normalize(shading.normal);
	vec3 d = CASTER_EYE - shading.position; // light's direction vector
	const float l = length(d);
	d = normalize(d);
	const float alpha = dot(n, d);

	diffuse *= max(alpha, 0.0);
	const uint raiseBy = 32;
	specular *= pow(max(dot(normalize(-shading.position), reflect(-d, n)), 0.0),
	                raiseBy);
	const float constant = 1.0, linear = 0.007, quadratic = 0.0002,
	            attenuation = 1.0 / (constant + linear * l + quadratic * (l * l));

	return vec4((diffuse + specular) * attenuation, (alpha < 0.0) ? 1.0 : 0.0);
}

//______________________________________________________________________________
void main(void)
{
	vec4 color = vec4(1.0);
	if (MAP_UV)
		color = texture(DIFFUSE, vert.uv);
	vec4 shade = phong();
	fragColor = color *
	            vec4(shade.xyz, 1.0) *
	            visibility(bool(shade.w));
}