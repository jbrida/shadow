// Ján Brída xbrida01 AT stud.fit.vutbr.cz

#version 420

in Vert {
	vec2 uv;
} vert;

in Shading {
	vec3 position, normal;
} shading;

in Shadow {
	vec4 p;
} shadow;

out vec4 fragColor;

uniform bool MAP_UV;
uniform sampler2D DIFFUSE, DEPTH_MAP;
uniform vec3 CASTER_EYE;

//______________________________________________________________________________
bool inLightSpace(const vec3 p)
{
	return p.x <= 1.0 && p.x >= -1.0 && p.y <= 1.0 && p.y >= -1.0;
}

//______________________________________________________________________________
float bin(const float d, const vec2 uv)
{
	const float bias = 0.006;
	return step(d - bias, texture(DEPTH_MAP, uv).r);
}

//______________________________________________________________________________
float bilinear(const float d, const vec2 uv)
{
	const vec2 texelSize = 1.0 / textureSize(DEPTH_MAP, 0),
	           pixelPos = uv / texelSize + vec2(0.5),
	           fractPart = fract(pixelPos),
	           startTexel = (pixelPos - fractPart) * texelSize;

	float bottomLeft = bin(d, startTexel),
	      bottomRight = bin(d, startTexel + vec2(texelSize.x, 0.0)),
	      topLeft = bin(d, startTexel + vec2(0.0, texelSize.y)),
	      topRight = bin(d, startTexel + texelSize);
	const float a = mix(bottomLeft, topLeft, fractPart.y),
	            b = mix(bottomRight, topRight, fractPart.y);

	return mix(a, b, fractPart.x);
}

//______________________________________________________________________________
float pcf(const float d, const vec2 uv)
{
	float sum = 0.0;

	const float width = 3.0,
	            edge = (width - 1.0) / 2.0;
	const vec2 texelSize = 1.0 / textureSize(DEPTH_MAP, 0);
	for (float y = -edge; y <= edge; y += 1.0)
		for (float x = -edge; x <= edge; x += 1.0)
			sum += bilinear(d, uv + vec2(x, y) * texelSize);

	return sum / pow(width, 2);
}

//______________________________________________________________________________
float visibility(const bool averted)
{
	float result = 0.2;

	const vec3 p = shadow.p.xyz / shadow.p.w;
	if (!averted && inLightSpace(p) && shadow.p.w > 0.0)
		result = pcf(p.z * 0.5 + 0.5, p.xy * 0.5 + 0.5);

	return result;
}

//______________________________________________________________________________
vec4 phong(void)
{
	vec3 diffuse = vec3(0.5),
	     specular = vec3(0.5);

	const vec3 n = normalize(shading.normal);
	vec3 d = CASTER_EYE - shading.position; // light's direction vector
	const float l = length(d);
	d = normalize(d);
	const float alpha = dot(n, d);

	diffuse *= max(alpha, 0.0);
	const uint raiseBy = 32;
	specular *= pow(max(dot(normalize(-shading.position), reflect(-d, n)), 0.0),
	                raiseBy);
	const float constant = 1.0, linear = 0.007, quadratic = 0.0002,
	            attenuation = 1.0 / (constant + linear * l + quadratic * (l * l));

	return vec4((diffuse + specular) * attenuation, (alpha < 0.0) ? 1.0 : 0.0);
}

//______________________________________________________________________________
void main(void)
{
	vec4 color = vec4(1.0);
	if (MAP_UV)
		color = texture(DIFFUSE, vert.uv);
	vec4 shade = phong();
	fragColor = color *
	            vec4(shade.xyz, 1.0) *
	            visibility(bool(shade.w));
}