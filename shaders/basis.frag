// Ján Brída xbrida01 AT stud.fit.vutbr.cz

#version 420

#define M 16
#define PI 3.1415926535897932384626433832795

out vec4 fragColor[M / 2];

uniform sampler2D INPUT;

//______________________________________________________________________________
void main(void)
{
	const ivec2 res = textureSize(INPUT, 0);
	const vec2 uv = vec2(gl_FragCoord.x / res.x, gl_FragCoord.y / res.y);
	const float z = texture(INPUT, uv).r;

	for (uint i = 0, k = 1; i < M / 2; i++, k += 2) {
		const vec2 ck = vec2(PI * (2 * k - 1), PI * (2 * (k + 1) - 1));
		fragColor[i] = vec4(sin(ck.x * z), cos(ck.x * z),
		                    sin(ck.y * z), cos(ck.y * z));
	}
}