// Ján Brída xbrida01 AT stud.fit.vutbr.cz

#version 420

out float fragColor;

//______________________________________________________________________________
void main(void)
{
	const uint c = 80;
	fragColor = exp(c * gl_FragCoord.z);
}