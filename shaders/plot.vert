// Ján Brída xbrida01 AT stud.fit.vutbr.cz

#version 420

layout (location = 0) in vec2 position;

uniform mat4 PROJ;

void main(void)
{
	gl_Position = PROJ * vec4(position.x, position.y, 0.0, 1.0);
}