// Ján Brída xbrida01 AT stud.fit.vutbr.cz

#version 420

out vec2 fragColor;

//______________________________________________________________________________
void main(void)
{
	const float dx = dFdx(gl_FragCoord.z),
	            dy = dFdy(gl_FragCoord.z);
	fragColor = vec2(gl_FragCoord.z, pow(gl_FragCoord.z, 2) + 0.25 * (pow(dx, 2) +
	                                 pow(dy, 2)));
}