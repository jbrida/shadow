// Ján Brída xbrida01 AT stud.fit.vutbr.cz

#version 420

in Vert {
	vec2 uv;
} vert;

in Shading {
	vec3 position, normal;
} shading;

in Shadow {
	vec4 p;
} shadow;

out vec4 fragColor;

uniform bool MAP_UV;
uniform sampler2D DIFFUSE, DEPTH_MAP;
uniform vec3 CASTER_EYE;

//______________________________________________________________________________
bool inLightSpace(const vec3 p)
{
	return p.x <= 1.0 && p.x >= -1.0 && p.y <= 1.0 && p.y >= -1.0;
}

//______________________________________________________________________________
// Light-bleeding reduction
float linStep(const float low, const float high, const float v)
{
	return clamp((v - low) / (high - low), 0.0, 1.0); // make sure to remain
	                                                  // in <0.0, 1.0>
}

//______________________________________________________________________________
float vsm(const float t, const vec2 uv)
{
	vec2 m = texture(DEPTH_MAP, uv).rg;
	float p = step(t, m.x);

	if (p == 0.0) {
		const float minVariance = 0.00002,
		            variance = max(m.y - m.x * m.x, minVariance),
		            d = t - m.x, // fraction of pixels over a filter region
		            pMax = variance / (variance + d * d), // upper bound
		            lightbleedingReduction = 0.2; // sets agressiveness
		p = linStep(lightbleedingReduction, 1.0, pMax);
	}

	return p;
}

//______________________________________________________________________________
float visibility(const bool averted)
{
	float result = 0.2;

	const vec3 p = shadow.p.xyz / shadow.p.w;
	if (!averted && inLightSpace(p) && shadow.p.w > 0.0)
		result = vsm(p.z * 0.5 + 0.5, p.xy * 0.5 + 0.5);

	return result;
}

//______________________________________________________________________________
vec4 phong(void)
{
	vec3 diffuse = vec3(0.5),
	     specular = vec3(0.5);

	const vec3 n = normalize(shading.normal);
	vec3 d = CASTER_EYE - shading.position; // light's direction vector
	const float l = length(d);
	d = normalize(d);
	const float alpha = dot(n, d);

	diffuse *= max(alpha, 0.0);
	const uint raiseBy = 32;
	specular *= pow(max(dot(normalize(-shading.position), reflect(-d, n)), 0.0),
	                raiseBy);
	const float constant = 1.0, linear = 0.007, quadratic = 0.0002,
	            attenuation = 1.0 / (constant + linear * l + quadratic * (l * l));

	return vec4((diffuse + specular) * attenuation, (alpha < 0.0) ? 1.0 : 0.0);
}

//______________________________________________________________________________
void main(void)
{
	vec4 color = vec4(1.0);
	if (MAP_UV)
		color = texture(DIFFUSE, vert.uv);
	vec4 shade = phong();
	fragColor = color *
	            vec4(shade.xyz, 1.0) *
	            visibility(bool(shade.w));
}