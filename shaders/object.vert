// Ján Brída xbrida01 AT stud.fit.vutbr.cz

#version 420

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec2 uv;

out Vert {
	vec2 uv;
} vert;

out Shading {
	vec3 position, normal;
} shading;

out Shadow {
	vec4 p;
} shadow;

uniform mat4 MVP, VIEWER, CASTER_MVP;
uniform mat3 NORMAL; // inverse transposed model-view matrix

//______________________________________________________________________________
void main(void)
{
	vert.uv = vec2(uv.x, 1.0 - uv.y);
	shading.position = (VIEWER * vec4(position, 1.0)).xyz;
	shading.normal = NORMAL * normal;
	shadow.p = CASTER_MVP * vec4(position, 1.0);

	gl_Position = MVP * vec4(position, 1.0);
}