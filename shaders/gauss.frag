// Ján Brída xbrida01 AT stud.fit.vutbr.cz

#version 420

out vec4 fragColor;

uniform sampler2D INPUT;
uniform vec2 BLUR;

//______________________________________________________________________________
void main(void)
{
	vec4 color = vec4(0.0);
	const ivec2 res = textureSize(INPUT, 0);
	const vec2 uv = vec2(gl_FragCoord.x / res.x, gl_FragCoord.y / res.y);

	color += texture(INPUT, uv + (vec2(-3.0) * BLUR)) * (1.0 / 64.0);
	color += texture(INPUT, uv + (vec2(-2.0) * BLUR)) * (6.0 / 64.0);
	color += texture(INPUT, uv + (vec2(-1.0) * BLUR)) * (15.0 / 64.0);
	color += texture(INPUT, uv + (vec2(0.0) * BLUR)) * (20.0 / 64.0);
	color += texture(INPUT, uv + (vec2(1.0) * BLUR)) * (15.0 / 64.0);
	color += texture(INPUT, uv + (vec2(2.0) * BLUR)) * (6.0 / 64.0);
	color += texture(INPUT, uv + (vec2(3.0) * BLUR)) * (1.0 / 64.0);

	fragColor = color;
}