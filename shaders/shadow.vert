// Ján Brída xbrida01 AT stud.fit.vutbr.cz

#version 420

layout (location = 0) in vec3 position;

uniform mat4 MVP;

//______________________________________________________________________________
void main(void)
{
	gl_Position = MVP * vec4(position, 1.0);
}