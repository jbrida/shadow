// Ján Brída xbrida01 AT stud.fit.vutbr.cz

#version 420

out float fragColor;

//______________________________________________________________________________
void main(void)
{
	fragColor = gl_FragCoord.z;
}